__author__ = 'ian'
import json
from subprocess import STDOUT,Popen, call, check_output, CalledProcessError
from flask import Flask, request, send_file, Response, render_template, current_app, url_for, make_response
import os
from base64 import urlsafe_b64encode
from flask_mail import Mail, Message
from smtplib import SMTPException, SMTPAuthenticationError, SMTPServerDisconnected
import shutil
import logging
import requests
import subprocess
import time

# configuration
DEBUG = True
SECRET_KEY = '9ddtd2mz%rr3@z+-nqwh@0jcu7#)0x$8fvblt1515tzl7k5=u!*!'
FILE_PATH = '/home/jane/exhibition/files/'
VIDEO_PATH = '/home/jane/exhibition/video/'
MAX_USERS = 200
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME = 'ianantonyflitman@gmail.com'
MAIL_PASSWORD = 'Jane16_61'

app = Flask(__name__)
app.config.from_object(__name__)
mail = Mail(app)

logging.basicConfig(filename='/home/jane/exhibition/log/jane.log', level=logging.DEBUG)
ffmpeg_handler = logging.FileHandler(filename='/home/jane/exhibition/log/ffmpeg_failure.log')
ffmpeg_handler.setLevel(logging.INFO)
creation_handler = logging.FileHandler(filename='/home/jane/exhibition/log/ffmpeg_creation.log')
creation_handler.setLevel(logging.INFO)
mail_handler = logging.FileHandler(filename='/home/jane/exhibition/log/mail.log')
mail_handler.setLevel(logging.INFO)
logging.getLogger('failure').addHandler(ffmpeg_handler)
logging.getLogger('creation').addHandler(creation_handler)
logging.getLogger('mail').addHandler(mail_handler)


@app.route('/favicon.ico')
def favicon():
    return send_file('static/images/favicon.ico', mimetype='image/vnd.microsoft.icon')


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        fileHandler = logging.FileHandler(log_file, mode='w')
        l.addHandler(fileHandler)
        return l


def space_available():
    dirnum = len(list(os.walk('files'))) - 1
    #print('dirnum {0}'.format(dirnum))
    if dirnum > MAX_USERS:
        return False
    else:
        return True


def search_error(output, filename, request_params):
    #print('output: {0}'.format(output))
    findno = output.find('Impossible to open')
    if findno != -1:
        endno = output.find('mp4', findno) + 4
        print 'error: {0}'.format(output[findno:endno])
        logging.getLogger('failure').info('{0}\t{1}\nrequest:\n{2}'.format(time.strftime("%Y-%m-%d %H:%M"), output[findno:endno], request_params))
        send_mail_error(output[findno:endno], ''.join('{}{}'.format(key, val) for key, val in request_params.items()))
    else:
        logging.getLogger('creation').info('{0}\t{1}'.format(time.strftime("%Y-%m-%d %H:%M"), filename))


def send_mail_error(msg, request_params):
    retcode = 0
    msg = Message(
            subject=msg,
            sender=MAIL_USERNAME,
            recipients=['ianflitman@gmail.com'],
            body=request_params
    )
    try:
        mail.send(msg)
    except SMTPAuthenticationError, e:
        retcode = 2
    except SMTPServerDisconnected, e:
        retcode = 3
    except SMTPException, e:
        retcode = 1

    return retcode

@app.route('/api/writeMovie', methods=['GET', 'POST'])
def write_movie():
    print(request.user_agent)
    if not space_available():
        resp = make_response('full', 302)
        return resp

    dataDict = json.loads(request.data)
    directory = FILE_PATH + dataDict['UID']
    print dataDict

    if not os.path.isdir(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            #print 'error while making a directory'
            logging.DEBUG('Error making directory {0} at {1}\nFailed because:'.format(directory, time.strftime("%Y-%m-%d %H:%M"), e.strerror))
            pass
    else:
        try:
            filelist = [f for f in os.listdir(directory)]
            for f in filelist:
                os.remove(directory + '/' + f)
        except OSError as e:
            logging.DEBUG('Failed removing: {0}\nFailed because: {1}'.format(', '.join(filelist), e.strerror))
            #print('failed removing :' + ', '.join(filelist))
            #print "Failed with:", e.strerror
            pass

    random_name = 'Jane_' + urlsafe_b64encode(os.urandom(6))
    file_base = directory + '/' + random_name
    concatFile = file_base + '.txt'
    movieFile = file_base + '.mp4'

    f = open(concatFile, 'w')
    for shot in dataDict['playlist']:
        f.write('file\t' + '\'' + VIDEO_PATH + shot + '.mp4' + '\'' + '\n')
    f.close()
    try:
        res = check_output(['ffmpeg', '-f', 'concat', '-i', concatFile, '-c', 'copy', '-y', movieFile], stderr=STDOUT)
    except subprocess.CalledProcessError, e:
        print(e.output)
        logging.DEBUG(e.output)
        pass

    search_error(res, random_name, dataDict)

    return random_name

#def
#def popen_thread(onExit, concatFile, movieFile):
#    def runInThread(onExit):
#        proc = subprocess.Popen(['ffmpeg', '-f', 'concat', '-i', concatFile, '-c', 'copy', '-y', movieFile])


@app.route('/api/deleteFolder', methods=['POST'])
def delete_folder():
    folder = request.json['UID']
    #print folder
    directory = FILE_PATH + folder
    if os.path.isdir(directory):
        try:
            shutil.rmtree(directory)
        except shutil.Error, e:
            print 'Error deleting directory. Error args: {0}'.format(e.args[0])
            logging.info('Error deleting directory:\t{0}\t{1}'.format(time.strftime("%Y-%m-%d %H:%M"), e.args[0]))
            pass

    return 'done'


@app.route('/api/email', methods=['POST'])
def email():
    dataDict = json.loads(request.data)
    print(dataDict)
    dataDict['secret'] = '6LdLOhATAAAAAOiOJ4heO5qCK7KpVIw6AHvNOjNd'
    data_params = {'secret': '6LdLOhATAAAAAOiOJ4heO5qCK7KpVIw6AHvNOjNd', 'response': dataDict['g-recaptcha-response']}
    #print json.dumps(data_params)
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data_params)
    body_msg = 'from: ' + dataDict['email'] + '\n' + dataDict['message']
    print body_msg
    retcode = 0
    if r.json()["success"]:
        msg = Message(
            subject=dataDict['subject'],
            sender=MAIL_USERNAME,
            recipients=['ianflitman@gmail.com'],
            body=body_msg
        )
        try:
            mail.send(msg)
        except SMTPAuthenticationError, e:
            retcode = 2
        except SMTPServerDisconnected, e:
            retcode = 3
        except SMTPException, e:
            retcode = 1
    else:
        retcode = 4

    #print(r.json())
    print('retcode: {0}'.format(retcode))
    logging.getLogger('mail').info('\n{0}\t Email\nmessage:\n{1}\nreturn code:{2}'.format(time.strftime("%Y-%m-%d %H:%M"), body_msg, str(retcode)))
    return str(retcode)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
    #sys.path.append('pycharm-debug.egg')
    #import pydevd
    #pydevd.settrace('localhost', port=8009, stdoutToServer=True, stderrToServer=True)
