import webkit
import gtk


class AutoCreate:

    def __init__(self):
        self.view = webkit.WebView()
        self.settings = webkit.WebSettings()
        self.view.set_settings(self.settings)
        self.w = gtk.Window()
        self.w.connect("destroy", gtk.main_quit)


        self.view.connect("load-finished", self.finished)
        self.view.connect('console-message', self.console_message)
        #self.view.open("http://localhost:8000/#/auto")
        self.w.add(self.view)
        # self.w.show_all() # the window is hidden!
        #gtk.main()
        #pass



    def console_message(self, view, message, line, source_id, data=None):
        print('message: {0}'.format(message))
       

    def finished(self, widget, frame):
        print 'load finished'

    def echo(self):
        return 'auto_window says hello'
        


