import subprocess
from subprocess import check_output, CalledProcessError, Popen, PIPE, STDOUT
import os


def create_window():
    try:
        #res = check_output(["Xvfb",  ":1", "-screen", "0", "1024x768x16"])
        command = ['/bin/bash', '/usr/bin/Xvfb', ':1', '-screen', '0', '1024x768x16', '&']
        #res = os.spawnl(os.P_NOWAIT, *command)
        #process = check_output(command, stderr=STDOUT)
        process = Popen(command, stdout=PIPE, stderr=PIPE)
        out, err = process.communicate()
        pass
    except os.error as err:
        print('xvfb do not work as expected')
        print(err.message)
        return 1


create_window()
