var janeApp = angular.module('janeApp', [
    'ngRoute',
    'ngResource',
    'ngAnimate',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'janeControllers',
    'scriptBoxWidgets',
    'janeFilters',
    'janeServices',
    'ngSanitize',
    'vcRecaptcha',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.buffering',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.poster',
    'youtube-embed'//,
    //'btford.socket-io'
]);


var folderID;

/*janeApp.factory('socket', function (socketFactory) {
  var socket = socketFactory();
  socket.forward('error');
  return socket;
});


janeApp.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});*/


janeApp.value('initData', {
    UID: 'exhibitionUser',//generateUUID(),
    autoID: 'autoUser',
    lastMovie: 'None',
    changed: false,
    ready: false,
    playlist: []
});

janeApp.config(['$routeProvider','$locationProvider',
    function($routeProvider, $locationProvider) {

        $routeProvider.
            when('/jane', {
                templateUrl: 'partials/script.html',
                controller: 'MasterScriptController'
            }).
            when('/auto', {
                templateUrl: 'partials/autoscript.html',
                controller: 'MasterScriptController'
            }).
            when('/intro', {
               templateUrl: 'partials/intro.html'
            }).
            when('/jane_structure', {
                templateUrl: 'partials/jane_structure.html'
            }).
            when('/me',{
                templateUrl: 'partials/me.html'
            }).
            when('/hackney', {
                templateUrl: 'partials/hackney_as.html'
            }).
            when('/people',{
                templateUrl: 'partials/people.html'
            }).
            when('/full', {
                templateUrl: 'partials/full.html'
            }).
            otherwise({
                redirectTo: '/jane'
            });

        //$locationProvider.html5Mode(true);
    }]);


function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    folderID = uuid;
    return uuid;
}

window.onbeforeunload = deleteServerFolder;

function deleteServerFolder(){
    $.ajax({
        url: "/api/deleteFolder",
        type: "POST",
        data: JSON.stringify({'UID': folderID}),
        contentType: 'application/json;charset=UTF-8',
        success: function(data){
            console.log('folder deleted: ' +  data);
        }
    })
}