/**
 * Created by ian on 14.02.15.
 */

var janeServices = angular.module('janeServices', ['ngResource']);

janeServices.factory('Scene',

    function($resource, $cacheFactory){
        var myCache = $cacheFactory('Scene');
        var title = "";

        var scene = $resource('json_model/:scene.json', {scene:'@scene'}, {
            query: {
                method:'GET',
                isArray: false,
                format: 'json',
                cache: true,

                transformResponse: function(data, header) {
                    var content =[];
                    var parts = [];
                    data = JSON.parse(data);
                    var cumulativeLength = 0;
                    for (var i = 0; i < data.scene.parts.length; i++) {
                        parts.push({'id': data.scene.code + '_' + data.scene.parts[i].keyword, 'length': data.scene.parts[i].content.length}); // 'length': data.scene.parts[i].content.length});
                        for (var a = 0; a < data.scene.parts[i].content.length; a++) {
                            data.scene.parts[i].content[a].index = cumulativeLength;
                            content.push(data.scene.parts[i].content[a]);
                            cumulativeLength++;
                        }
                    }
                    
                    myCache.put('script', content);
                    myCache.put('parts', parts);
                    myCache.put('duration', 0);
                    title = data.scene.name;
                    return {content:content, title:title};
                }
            }
        });
        return scene;
    }
);

janeServices.factory('AutoGen', ['$cacheFactory','Play','initData', '$rootScope','$location',
    function($cacheFactory, Play, initData, $rootScope, $location){
        var num = 0;
        var cut_ints = [];
        return function(cut_int){
            num++;
            cut_ints.push(cut_int);

            var myCache = $cacheFactory.get('Scene');
            //var cutNum = myCache.get('script').length;
            //console.log(cut_ints.sort(function(a, b){return a-b}).toString() +':'+ num);
            if (num == myCache.get('script').length){
                initData.ready = true;
                initData.changed = false;
                var firstShot = $('#row_0').attr('data-file').split(',')[0];
                var firstCode = firstShot.substr(firstShot.lastIndexOf('_')+1,2);
                $rootScope.$broadcast('getPoster', firstCode);
                num = 0;
                cut_ints = [];
                console.log('location is: ' + $location.path());
                //if($location.path() == '/auto'){
                    console.log('auto creation in full effect');
                    Play()
                //}
            }
        }
}]);


janeServices.factory('Play', ['$http', '$cacheFactory', '$compile', '$rootScope','initData', '$location',

    function( $http, $cacheFactory, $compile, $rootScope, initData, $location){
      console.log('Play service');
        console.log('location: ' + $location.path());
        return function(autoplay){
            var data = $cacheFactory.get('Scene').get('script');
            var parts = $cacheFactory.get('Scene').get('parts');
            var cumulativeTotal = 0;
            var playList = [];

            for(var b=0; b<parts.length;b++){
                var converstationFolder = parts[b].id.substr(0,parts[b].id.indexOf('_'));
                var partFolder = parts[b].id.substr(parts[b].id.indexOf('_')+1);
                for(var c = cumulativeTotal; c < cumulativeTotal + parts[b].length; c++){
                    var $currentRow = $('#row_' + data[c].index);
                    var files = $currentRow.attr('data-file').split(',');
                    for(var i=0; i < files.length; i++){
                        playList.push(converstationFolder + '/' + partFolder + '/' + files[i]);
                    }
                }
                cumulativeTotal +=parts[b].length;
            }

            //console.log(JSON.stringify(playList));
            //console.log(initData.UID);
            initData.playlist = playList;
            //http://178.79.184.214

            var directory = $location.path() != '/auto'? initData.UID: initData.autoID;

            var res = $http.post('/api/writeMovie', JSON.stringify({playlist: playList, UID: directory, lastMovie:initData.lastMovie}), {headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin':'*'}});
            res.success(function(data, status, headers, config) {
                console.log(data);
                console.log(headers);
                initData.changed = false;
                initData.lastMovie = data;
                $rootScope.$broadcast('movieReady', data, autoplay);
                console.log('auto movie created in ' + directory);
		    });
            res.error(function(data, status, headers, config) {
                if(status == 302){
                    $location.path('/full')
                }
			    console.log( "failure message: " + JSON.stringify({data: data, status: status, headers: headers, conig:config}));
		    });
        }
    }]
);

janeServices.factory('LinkToCut',
    function($resource, $cacheFactory){
        return function(link) {
            var parts = $cacheFactory.get('Scene').get('parts');
            var partid = link.substr(0, link.lastIndexOf('_'));
            var cutIndex = Number(link.substr(link.lastIndexOf('_')+1));
            var cumulativeTotal = 0;
            for(var a = 0; a < parts.length; a++){
                if(parts[a].id == partid){
                    return cumulativeTotal + cutIndex;
                }
                cumulativeTotal += parts[a].length;
            }
        }
    }
);

janeServices.factory('PairColour', ['LinkToCut','$cacheFactory', function(LinkToCut, $cacheFactory){

    var pairCols = [{'bg':'#296e1b', 'flash':'#cfedc9'},
        {'bg':'#823130', 'flash': '#e5b3b4'},
        {'bg':'#2b5085', 'flash':'#b4cdec'},
        {'bg':'#877126', 'flash': '#e6dab0'},
        {'bg':'#6e2982', 'flash': '#e1b1ed'}];

    var currentColourIndex = 0;
    var pairings = {};

    return {
        getColours: function(index, pos, total) {
            var callerIndex = index;
            if (!(index in pairings)) {
                var data = $cacheFactory.get('Scene').get('script');
                var originalIndex = index;
                pairings[index] = currentColourIndex;

                if (pos == 1) {
                    while (pos < total) {
                        index = LinkToCut(data[index].arguments.next);
                        pairings[index] = currentColourIndex;
                        pos++;
                    }
                } else {
                    var originalPos = pos;
                    while (pos > 1) {
                        index = LinkToCut(data[index].arguments.prev);
                        pairings[index] = currentColourIndex;
                        pos--;
                    }

                    while (originalPos < total) {
                        originalIndex = LinkToCut(data[originalIndex].arguments.next);
                        pairings[index] = currentColourIndex;
                        originalPos++;
                    }
                }

                if (currentColourIndex < 4) {
                    currentColourIndex++;
                } else {
                    currentColourIndex = 0;
                }
            }

            return pairCols[pairings[callerIndex]];
        },
        reset : function(){
            pairings ={}
        }
    }
}]);

janeServices.factory('DurationControl',
    function($resource, $cacheFactory){
        var length = $cacheFactory.get('Scene').get('duration');
        var myCache = $cacheFactory('Scene');
        return function(time){
            length += time;
            myCache.put('duration', length);
        }
    }
);


