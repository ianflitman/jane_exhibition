/**
 * Created by ian on 06.02.15.
 */

var janeControllers = angular.module('janeControllers', []);


janeControllers.controller('MasterScriptController',[ '$scope', '$rootScope', '$http', 'Scene', 'PairColour','initData', '$interval',
    function ($scope, $rootScope, $http, Scene, PairColour, initData, $interval) {
        var conversations = ['mtl', 'sc', 'ofne', 'bs', 'eieo', 'ff'];
        var conversationChoice = conversations[Math.floor(Math.random() * conversations.length)];
        console.log('random conversation: ' + conversationChoice);
        var data = Scene.query({scene: conversationChoice});

        data.$promise.then(function (data) {
            $scope.content = function () {
                return data.content;
            };

            $scope.getPartName = function (which) {
                if (which > data.scene.parts.length - 1) {
                    which = 0;
                }
                return data.scene.parts[which].name;
            };

            $scope.title = data.title;
            $scope.generated =  new Date().getTime();//toUTCString();
            initData.changed = false;
            console.log(conversationChoice)
            $rootScope.$broadcast('conversationGenerated', conversationChoice);

        });

        $scope.$on('changeConversation', function(e, data) {
            data = Scene.query({scene: data});
            data.$promise.then(function (data) {
                PairColour.reset();
                $scope.generated =  new Date().getTime();//toUTCString();
                $scope.content = function () {
                    return data.content;
                };
            })
        });

        $scope.$on('rebuild', function(e, cameraChanges, dialogueChanges){
            console.log('initData.changed MasterCtrl rebuild : ' + initData.changed);
            //var j = JSON.parse(data)
            console.log('message data: camera' + cameraChanges + ' dialogue: ' + dialogueChanges)
            //
            //if(j['cameraChanges']==0 && j['dialogueChanges']==0){
            if((cameraChanges==0) && (dialogueChanges ==0) ){
            //if(!initData.changed) { //$scope.$root.cameraChanges ==0 && $scope.$root.dialogueChanges == 0) {

                var conversations = ['mtl', 'sc', 'ofne', 'bs', 'eieo', 'ff'];
                var conversationChoice = conversations[Math.floor(Math.random() * conversations.length)];
                console.log('random conversation master cmd rebuild: ' + conversationChoice);
                var data = Scene.query({scene: conversationChoice});

                data.$promise.then(function (data) {
                    $scope.content = function () {
                        return data.content;
                    };

                    $scope.getPartName = function (which) {
                        if (which > data.scene.parts.length - 1) {
                            which = 0;
                        }
                        return data.scene.parts[which].name;
                    };

                    $scope.title = data.title;
                    $scope.generated = new Date().getTime();//toUTCString();
                    initData.changed = false;
                    $rootScope.$broadcast('conversationGenerated', conversationChoice);

                });
            }
        })

        $scope.checkGeneration = function(){
            console.log($scope.generated)
            var timeDiff = new Date().getTime() - $scope.generated;
            console.log('generated last ago : ' + timeDiff);
            if(timeDiff > 720000){
                $scope.$root.$broadcast('resetChanges');
                initData.changed = false;
                $scope.$root.$broadcast('rebuild', 0, 0);
            }
        };
        $interval(function(){$scope.checkGeneration();}, 300000)
    }
]);

janeControllers.controller('ChangesController', ['$rootScope',
    function($rootScope){
        $rootScope.cameraChanges = 0;
        $rootScope.dialogueChanges = 0;
    }
]);

janeControllers.controller('NavCtrl', ['$scope', '$location',function($scope, $location) {

    $scope.goPage = function (page) {
        $location.path('/jane');
    }
}]);

janeControllers.controller('CaptchaCtrl', ['$scope','vcRecaptchaService','$http',
    function ($scope, vcRecaptchaService, $http) {
        $scope.showFeedback=false;
        $scope.email = '';
        $scope.subject= '';
        $scope.message = '';
        $scope.publickey = '6LdLOhATAAAAAE2QC-BpE1FOBUlXQZGoP0UbIDe_';
        $scope.checkEmail = function(){
            console.log('checking email');
            if(vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please answer the captcha question before sending")
            }else{
                var post_data = {
                    'email': $scope.email,
                    'subject': $scope.subject,
                    'message': $scope.message,
                    'g-recaptcha-response': vcRecaptchaService.getResponse()
                };
                console.log(vcRecaptchaService.getResponse());

                var $feedback = $('#emailFeedback');

                $http.post('/api/email',post_data).success(function(response){
                    console.log('response: ' + response);
                    if(response === '0'){
                        $feedback.addClass('alert-success');
                        $feedback.text("The email was successfully sent.");
                        $scope.showFeedback = true;
                    }else{
                        $feedback.addClass('alert-danger');
                        $feedback.text("Something went wrong so the email was not sent.");
                        $scope.showFeedback = true;
                    }
                }).error(function(error){
                    $feedback.addClass('alert-danger');
                    $feedback.text("Something went wrong so the email was not sent.");
                    $scope.showFeedback = true;
                })
            }
        }
    }
]);

janeControllers.controller('TabCtrl', function($scope, $window){
    $scope.ofne_1 = 'f_MnubWCiCU';
    $scope.ofne_2 = 'AUTNIsM9KWM';
    $scope.ofne_3 = 'Ys-oyUANOJ4';
    $scope.bs_1 = 'pGyjXtbTpo0';
    $scope.bs_2 = 'kAYMoYzUjIE';
    $scope.bs_3 = 'JFmoMf9F6Tk';
    $scope.mtl_1 = 'VDL7EvPwqew';
    $scope.mtl_2 = 'uZcFzhNIemU';
    $scope.mtl_3 = 'LdwBGMQ_xFU';
    $scope.sc_1 = 'bztJGIwqFFU';
    $scope.sc_2 = 'qQQe8f78mv4';
    $scope.sc_3 = 'o4m5BSRPnbs';
    $scope.eieo_1 = 'l7yLJTcnOTE';
    $scope.eieo_2 = 'odNZrUUGN48';
    $scope.eieo_3 = 'uTpeX6rGfBE';
    $scope.ff_1 = 'UAoYdX0ptUs';
    $scope.ff_2 = 'Q-Csp-ntJPc';
    $scope.ff_3 = 'uNePSXdcnj8';
});

//janeControllers.controller('AutoCtrl', ['$sce', function($sce){
 //   $scope.autoURL = {domain: $ace.tr}
//}]);

janeControllers.controller('VideoCtrl', ['$sce','$scope', '$timeout', 'initData', 'Play', '$location', function($sce, $scope, $timeout, initData, Play, $location ){
    var controller = this;
    $scope.poster = 'images/2a.png';

    controller.API = null;
    controller.onPlayerReady = function(API) {
        console.log('player ready..');
        controller.API = API;

        //controller.config.sources =[{src: $sce.trustAsResourceUrl("http://178.79.184.214/output.mp4"), type: "video/mp4"}];
        //$timeout(controller.API.play.bind(controller.API), 100);
    };

    $scope.movieGenerated = function(val){
        //return val;
        return initData.ready;
    };

    $scope.vgError = function(event) {
     console.log("onVgError: " + event);
    };

    $scope.$on('getPoster', function(event, shot){
        var imgStr  = 'images/' + shot + '.jpg';
        controller.config.plugins = {poster: imgStr};
        $scope.$apply();
    });

    //$scope.$on('rebuild', function(event){
        //console.log('rebuild message in controller');
        //$location.path('/jane');
        //Play()
    //});

    $scope.$on('incrementDialogueChanges', function(e, data){
        console.log('dialog change in videogular controller');
        controller.API.pause();
            /*if(this.currentState == VG_STATES.PLAY){
                console.log('stopping')
                this.setState(VG_STATES.STOP);
                $scope.$apply();
            }*/
        });

     $scope.$on('incrementCameraChanges', function(e, data){
         console.log('dialog change in videogular controller');
        controller.API.pause();
            /*if(this.currentState == VG_STATES.PLAY){
                console.log('stopping')
                this.setState(VG_STATES.STOP);
                $scope.$apply();
            }*/
        });

    $scope.$on('movieReady', function(event,movieName, autoPlay){
        console.log('heard that ' + movieName + ' is ready to play in VideoCtrl!');
        var movieUrl = "http://172.17.0.2/" + initData.UID + "/" + movieName + ".mp4";
        controller.config.sources =[{src: $sce.trustAsResourceUrl(movieUrl), type: "video/mp4"}];
        //if(autoPlay)
            $timeout(controller.API.play.bind(controller.API), 100);
        //$('#videoPlayer').css('display', 'inline-block');
    });

    this.config = {
        sources: [
            {src: $sce.trustAsResourceUrl("http://172.17.0.2/black.mp4"), type: "video/mp4"}//,
        ],
        theme: "vg/videogular-themes-default/videogular.css"
    };
}]);

