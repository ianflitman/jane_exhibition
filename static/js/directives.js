var scriptBoxWidgets = angular.module('scriptBoxWidgets', ['ui.bootstrap', 'janeFilters']);

var capitaliseFirstLetter = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
};

var toggleCollapse = function (val, $scope) {
    $('#collapse_' + val).collapse('toggle');
    var $icon = $('#icon_' + val + '> i');
    if ($icon.hasClass('fa-chevron-right')) {
        $icon.addClass('fa-chevron-down').removeClass('fa-chevron-right');
        $('#row_' + $scope.cut.index).addClass('placementborder');
        $('#row_' + ($scope.cut.index + 1)).addClass('nextrowline');
    } else {
        $icon.addClass('fa-chevron-right').removeClass('fa-chevron-down');
        $('#row_' + $scope.cut.index).removeClass('placementborder');
        $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
    }
};

var cameraTxtFromCode = function (input) {
    var camera_code_start = input.lastIndexOf('_') + 1;
    var camera_code = input.substr(camera_code_start);
    var camtext = "";

    switch (camera_code) {
        case 'co':
        case 'ca':
        case 'cn':
        case 'ce':
            camtext = 'near';
            break;
        case '2o':
        case '2s':
        case '2a':
            camtext = 'wide';
            break;
        default:
            camtext = 'not sure';
    }

    return camtext;
};

var setCamera = function ($camera, sources) {
    var camera_num = sources.length;
    var chosen_camera = Math.floor(Math.random() * camera_num);
    var source = sources[chosen_camera];
    var camtext = cameraTxtFromCode(source.file);
    cameraCss($camera, camtext);
    $camera.text(camtext);

    if (camera_num == 1)$camera.attr('disabled', true);

    return chosen_camera;
};

var cameraCss = function ($camera, camtext) {
    if (camtext.indexOf('wide') > -1) {
        if ($camera.hasClass('closecam')) {
            $camera.removeClass('closecam');
        }
        $camera.addClass('widecam')
    } else {
        if ($camera.hasClass('widecam')) $camera.removeClass('widecam');
        $camera.addClass('closecam')
    }
};

var cameraToggle = function (sources, index, $camera, $row) {
    var newIndex = (index == 0)? 1: 0;
    var filename = sources[newIndex].file;
    var camtext = cameraTxtFromCode(filename);
    $camera.text(camtext);
    cameraCss($camera, camtext);
    $row.attr('data-file', filename);
    return newIndex;
};

var getSeqFiles = function (input) {
    var files = [];
    for (var a = 0; a < input.length; a++) {
        files.push(input[a].file)
    }
    return files.toString();
};

var rowEnter = function (rowArray) {
    return function () {
        for (var i = 0, len = rowArray.length; i < len; i++) {
            rowArray[i].css('background-color', '#ebeded');
        }
    };
};

var rowLeave = function (rowArray) {
    return function () {
        for (var i = 0, len = rowArray.length; i < len; i++) {
            rowArray[i].css('background-color', '#fbfdff');
        }
    }
};

scriptBoxWidgets.directive('ctoriaFree', ['cameraFilter', 'speakerFilter', 'AutoGen', function (camera, speaker, AutoGen) {

    var autoSelect = function(scope, isReset){
        var $row = $('#row_' + scope.cut.index);
        var $choice = $("[data-cut-id=" + scope.cut.index + "]");
        var $camera = $('#camera_' + scope.cut.index);
        if(isReset){
            $('#' + scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
        }

        scope.nestedActive = false;
        if(scope.cut.type == 'WEEKDAY'){
            scope.selected = new Date().getDay() + 1;
        }else{
            scope.selected = Math.floor((Math.random() * scope.cut.options.length)) + 1;
        }

        $choice.attr('data-selected-position', scope.selected);
        var sources = scope.cut.options[scope.selected - 1].sources;
        scope.src_index = setCamera($camera, sources);
        $row.attr('data-file', sources[scope.src_index].file);
        var $option = $('#' + scope.cut.index + '_opt_' + scope.selected);
        $choice.text($option.text());
        $option.prop("disabled", true).toggleClass('optionbtn-disabled');

        AutoGen(scope.cut.index);
    };

    return {
        restrict: 'E',
        scope: {
            src_index: '@',
            src_index_other : '@',
            file: '@dataFile',
            selected: '@dataSelectedPosition',
            otherSelected: '@',
            cut: '=',
            nestedActive: '@'
        },
        templateUrl: 'templates/ctoria-free.html',
        controller: function ($scope, $element) {

            $scope.$on('reset', function(){
                autoSelect($scope, true);
            });

            $scope.fold = function (val) {
                toggleCollapse(val, $scope, true);
            };

            $scope.nestedChosen = function(){
                return $scope.nestedActive
            };

            $scope.optionClick = function (val, pos) {
                var $camera = $('#camera_' + $scope.cut.index);
                var $option = $('#' + val);
                var opt_txt = $option.text();
                $scope.selected = $option.attr('data-position');
                var $row = $('#row_' + $scope.cut.index);
                var sources = $scope.cut.options[$scope.selected - 1].sources;

                //if has one angles while the others have two
                //disable button and file text
                if(sources.length==1 && !$camera.attr('disabled')){
                    $scope.src_index = 0;
                    var camtext = cameraTxtFromCode(sources[0].file);
                    cameraCss($camera, camtext);
                    $camera.text(camtext);
                    $camera.attr('disabled', true)

                }
                //if has two angles but current choice has one and therefore a disabled camera button
                //enable the button and update its label accordingly
                if((sources.length ==2) && ($camera.attr('disabled'))){
                    var camtext = cameraTxtFromCode(sources[0].file);
                    cameraCss($camera, camtext);
                    $camera.text(camtext);
                    $camera.attr('disabled', false);
                }

                $row.attr('data-file', sources[$scope.src_index].file);

                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                $speaker.text(speaker($scope.cut.index));
                $choice.text(opt_txt);
                if($scope.nestedActive){
                    $scope.nestedActive = false;
                    $("#def_" + $scope.cut.index + "_child_1").css('color', '#333');
                }
                $scope.$root.$broadcast('incrementDialogueChanges');

                $('#collapse_' + $scope.cut.index).collapse('toggle');
                $('#icon_' + $scope.cut.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                if ($choice.attr('data-selected-position') != 0) {
                    $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
                    $row.removeClass('placementborder');
                    $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
                }
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.attr('data-selected-position', $option.attr('data-position'));
            };

            if($scope.cut.nested){
                $scope.src_index_other = -1;
                $scope.$on($scope.cut.index + '_childSelected', function(e, val, pos) {
                    $scope.otherSelected = Number(pos);
                    var $row = $('#row_' + $scope.cut.index);
                    var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                    $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
                    $choice.attr('data-selected-position', $scope.cut.options.length +1);
                    var $def = $("#def_" + $scope.cut.index + "_child_1");
                    $def.css('color', '#ed6a43');
                    var choicefile = $scope.cut.nested.default.sources[$scope.src_index].file;
                    var $selcamera = $('#selcamera_' + $scope.cut.index);

                    var selsources = $scope.cut.nested.options[$scope.otherSelected -1].sources;
                    if($scope.src_index_other == -1){
                        $scope.src_index_other = setCamera($selcamera, selsources);
                    }

                    $choice.text($def.text());
                    var $option = $('#' + val);
                    var opt_txt = $option.text();
                    var $selchoice = $("[data-cut-id=sel_" + $scope.cut.index +"]");
                    $selchoice.text(opt_txt);
                    $row.attr('data-file', choicefile + ',' + selsources[$scope.src_index_other].file);
                    $row.removeClass('placementborder');
                    $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
                    $scope.nestedActive = true;
                    $('#collapse_' + $scope.cut.index).collapse('toggle');
                    $('#icon_' + $scope.cut.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                });
            }
        },

        link: function (scope, $element, $attr) {

            $element.ready(function () {
                var $row = $('#row_' + scope.cut.index);
                var $choicerow = $('#choicerow_' + scope.cut.index);
                scope.selected = Math.floor((Math.random() * scope.cut.options.length)) + 1;
                var $choice = $("[data-cut-id=" + scope.cut.index + "]");
                var $speaker = $("[speaker-cut-id=" + scope.cut.index + "]");
                var $camera = $('#camera_' + scope.cut.index);

                if(scope.cut.nested){
                    var $selcamera = $('#selcamera_' + scope.cut.index);
                    $selcamera.on('click', function () {
                        var $selrow = $('#selrow_' + scope.cut.index);
                        var sources = scope.cut.nested.options[scope.otherSelected - 1].sources;
                        var datafiles = $row.attr('data-file').split(',');
                        scope.src_index_other = cameraToggle(sources, scope.src_index_other, $selcamera, $selrow);
                        datafiles[1] = sources[scope.src_index_other].file;
                        $row.attr('data-file', datafiles.toString());
                        scope.$root.$broadcast('incrementCameraChanges');
                    });
                }

                $speaker.text(speaker(scope.cut.index));
                autoSelect(scope, false);

                $choicerow.on('mouseenter', rowEnter([$choicerow, $choice]));
                $choicerow.on('mouseleave', rowLeave([$choicerow, $choice]));
                $camera.on('click', function () {
                    if(scope.nestedActive){
                        var $choicerow = $('#choicerow_' + scope.cut.index);
                        var defsources = scope.cut.nested.default.sources;
                        var datafiles = $row.attr('data-file').split(',');
                        scope.src_index = cameraToggle(defsources, scope.src_index, $camera, $choicerow);
                        datafiles[0] = defsources[scope.src_index].file;
                        $row.attr('data-file', datafiles.toString());
                    }else{
                        var sources = scope.cut.options[scope.selected - 1].sources;
                        scope.src_index = cameraToggle(sources, scope.src_index, $camera, $row);
                    }
                    scope.$root.$broadcast('incrementCameraChanges');
                });
            });
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaPairedChildFree', ['cameraFilter', 'speakerFilter', function (camera, speaker, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            selected: '@dataSelectedOption',
            index: '=',
            cut: '='
        },
        templateUrl: 'templates/ctoria-paired-child-free.html',
        controller: function ($scope, $element, $rootScope) {

            $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                } else {
                    $('#icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                }
            };

            $scope.optionClick = function (val) {
                var $child_choice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                var $option = $('#' + val);
                var oldSelected = $child_choice.attr('data-selected-option');

                var opt_txt = $option.text();
                $child_choice.text(opt_txt);

                //$('#collapse_' + $scope.index + '_child_' + $scope.cut.position).collapse('toggle');
                //$('#icon_' + $scope.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                //$('#icon_' + $scope.index + '_child_' + $scope.cut.position + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + oldSelected).css('color', '#333');
                $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected).prop("disabled", false).toggleClass('optionbtn-disabled');
                $scope.selected = Number($option.attr('data-position'));
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');

                $child_choice.attr('data-selected-option', $option.attr('data-position'));

                if($scope.$parent.cut.nested){
                    $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.$parent.cut.options.length + 1, $scope.$parent.cut.arguments.pos);
                }else if($scope.$parent.cut.type == 'ALTERNATIVE_NESTED'){
                    $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.selected, $scope.$parent.cut.arguments.pos, $scope.cut.position);
                }else{//ALTERNATIVE_PAIRED
                    $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.cut.position, $scope.$parent.cut.arguments.pos);
                }
            };

            $element.ready(function () {
                var $icon = $('#icon_' + $scope.index + '_child_' + $scope.cut.position);
                $scope.selected = Math.floor((Math.random() * $scope.cut.options.length) + 1);
                var $child_choice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                $child_choice.attr('data-selected-option', $scope.selected);
                var $camera = $('#camera_' + $scope.index);
                var sources = $scope.cut.options[$scope.selected - 1].sources;
                $scope.src_index = setCamera($camera, sources);
                var $option = $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected);
                var opt_txt = $option.text();
                $child_choice.text(opt_txt);
                $option.css('color', '#ed6a43');
                $child_choice.on('mouseenter', rowEnter([$child_choice, $icon]));//was $row
                $child_choice.on('mouseleave', rowLeave([$child_choice, $icon]));//was $row

                $rootScope.$broadcast($scope.index + '_childReady');
            });
        },

        link: function (scope, $element, $attr, $rootScope) {

        }
    }
}]);

scriptBoxWidgets.directive('ctoriaDefault', ['cameraFilter', 'AutoGen', function (camera, AutoGen) {
     var autoSelect = function(scope){
            var $row = $('#row_' + scope.cut.index);
            var $camera = $('#camera_' + scope.cut.index);
            var sources = scope.cut.sources;
            scope.src_index = setCamera($camera, sources);
            $row.attr('data-file', sources[scope.src_index].file);
            AutoGen(scope.cut.index);
     };
    return {
        restrict: 'E',
        scope: {
            src_index: '@',
            cut: '='
        },
        templateUrl: 'templates/ctoria-default.html',
        controller: function ($scope, $element) {
            $scope.$on('reset', function(){
                autoSelect($scope)
            })
        },
        link: function (scope, $element, $attr) {
            $element.ready(function () {
                autoSelect(scope);
                var $row = $('#row_' + scope.cut.index);
                var $camera = $('#camera_' + scope.cut.index);
                $camera.on('click', function () {
                    scope.src_index = cameraToggle(scope.cut.sources, scope.src_index, $camera, $row);
                    scope.$root.$broadcast('incrementCameraChanges');
                });
            });
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaSeqSet', ['seqCameraFilter', 'AutoGen', function (seqCamera, AutoGen) {
    var autoSelect = function(scope, isReset){
        var $choice = $("[data-cut-id=" + scope.cut.index + "]");
        var $cameraDesc = $("[camera-cut-id=" + scope.cut.index + "]");
        var setNames = ["short", "medium", "long"];
        var setChosen = Math.floor(Math.random() * 3);
        var seqChosen = Math.floor(Math.random() * scope.cut.sets[setChosen].seqs.length);
        var cameraVals = seqCamera(scope.cut.sets[setChosen].seqs[seqChosen]);

        scope.setChoice = setChosen;
        scope.seqChoice = seqChosen;
        var oldSetName = scope.setName;
        scope.setName = setNames[setChosen];
        if (isReset){
            var oldSelection = $choice.attr('data-selected-position');
            $('#' + oldSelection).prop("disabled", false).toggleClass('optionbtn-disabled');
            if(oldSetName != scope.setName){
                    $('[set-cut-id=' + scope.cut.index + '_seq_' + oldSetName + ']').css('color', '#333');
                    $('[set-cut-id=' + scope.cut.index + '_seq_' + scope.setName + ']').css('color', '#ed6a43');
            }
        }
        $('[set-cut-id=' + scope.cut.index + '_seq_' + scope.setName + '] > button').css('color', '#ed6a43');
        $('#' + scope.cut.index + '_' + setNames[setChosen] + '_' + seqChosen).prop("disabled", true).toggleClass('optionbtn-disabled');
        $choice.text(capitaliseFirstLetter(setNames[setChosen]) + ' pause');
        $choice.attr('data-selected-position', scope.cut.index + '_' + setNames[setChosen] + '_' + seqChosen);
        $('#row_' + scope.cut.index).attr('data-file', getSeqFiles(scope.cut.sets[setChosen].seqs[seqChosen]));
        $cameraDesc.text(cameraVals.camera);
        AutoGen(scope.cut.index);
    };

    return {
        restrict: 'E',
        scope: {
            cut: '=',
            dataSelectedPosition: '@',
            setName: '@'
        },
        templateUrl: 'templates/ctoria-seq-set.html',
        controller: function ($scope, $element, $document) {
            $scope.$on('reset', function(){
                autoSelect($scope, true);
            });
            $scope.pauseClick = function (setName, seqChosen) {
                if(setName != $scope.setName){
                    $('[set-cut-id=' + $scope.cut.index + '_seq_' + $scope.setName + '] > button').css('color', '#333');
                    $scope.setName = setName;
                    $('[set-cut-id=' + $scope.cut.index + '_seq_' + $scope.setName + '] > button').css('color', '#ed6a43');
                }
                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                var $row = $('#row_' + $scope.cut.index);
                var $pauseOpt = $('#' + $scope.cut.index + '_' + setName + '_' + seqChosen);
                var pauseTxt = $pauseOpt.attr('set-name');
                $choice.text(pauseTxt);
                $("[camera-cut-id=" + $scope.cut.index + "]").text($pauseOpt.text());
                var oldSelection = $choice.attr('data-selected-position');
                $choice.attr('data-selected-position', $scope.cut.index + '_' + setName + '_' + seqChosen);
                $('#collapse_' + $scope.cut.index).collapse('toggle');
                $scope.foldSeq(setName);
                $('#icon_' + $scope.cut.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                $pauseOpt.prop("disabled", true).toggleClass('optionbtn-disabled');
                $('#' + oldSelection).prop("disabled", false).toggleClass('optionbtn-disabled');
                $row.removeClass('placementborder');
                $row.attr('data-file', $pauseOpt.attr('data-file'));
                $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');

                $scope.$root.$broadcast('incrementCameraChanges', true);
            };

            $scope.fold = function (val) {
                toggleCollapse(val, $scope)
            };

            $scope.foldSeq = function (seq_name) {
                $('#collapse_' + $scope.cut.index + '_seq_' + seq_name).collapse('toggle');
                var $row = $('#row_' + $scope.cut.index + '_seq_' + seq_name);
                var $icon = $('#icon_' + $scope.cut.index + '_seq_' + seq_name + '> i');
                if ($icon.hasClass('fa-chevron-right')) {
                    $icon.addClass('fa-chevron-down').removeClass('fa-chevron-right');
                    $row.addClass('seqsetline');
                } else {
                    $icon.addClass('fa-chevron-right').removeClass('fa-chevron-down');
                    $row.removeClass('seqsetline');
                }
            };
        },
        link: function (scope, $element, $attr) {
            $element.ready(function () {
                var $choice = $("[data-cut-id=" + scope.cut.index + "]");
                var $cameraDesc = $("[camera-cut-id=" + scope.cut.index + "]");
                var $row = $('#row_' + scope.cut.index);
                $row.on('mouseenter', rowEnter([$row, $choice]));
                $row.on('mouseleave', rowLeave([$row, $choice]));

                var $shorticon = $('#icon_' + scope.cut.index + '_seq_' + scope.cut.sets[0].name);
                var $shortbut = $('[set-cut-id=' + scope.cut.index + '_seq_' + scope.cut.sets[0].name + ']');
                var $mediumicon = $('#icon_' + scope.cut.index + '_seq_' + scope.cut.sets[1].name);
                var $mediumbut = $('[set-cut-id=' + scope.cut.index + '_seq_' + scope.cut.sets[1].name + ']');
                var $longicon = $('#icon_' + scope.cut.index + '_seq_' + scope.cut.sets[2].name);
                var $longbut = $('[set-cut-id=' + scope.cut.index + '_seq_' + scope.cut.sets[2].name + ']');

                $shortbut.on('mouseenter', rowEnter([$shortbut, $shorticon]));
                $shortbut.on('mouseleave', rowLeave([$shortbut, $shorticon]));
                $mediumbut.on('mouseenter', rowEnter([$mediumbut, $mediumicon]));
                $mediumbut.on('mouseleave', rowLeave([$mediumbut, $mediumicon]));
                $longbut.on('mouseenter', rowEnter([$longbut, $longicon]));
                $longbut.on('mouseleave', rowLeave([$longbut, $longicon]));

                $cameraDesc.on('mouseenter', rowEnter([$row, $choice]));
                $cameraDesc.on('mouseleave', rowLeave([$row, $choice]));

                var setNames = ["short", "medium", "long"];
                var setChosen = Math.floor(Math.random() * 3);
                var seqChosen = Math.floor(Math.random() * scope.cut.sets[setChosen].seqs.length);
                var cameraVals = seqCamera(scope.cut.sets[setChosen].seqs[seqChosen]);

                autoSelect(scope, false);
            })
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaParent', ['cameraFilter', '$compile', 'AutoGen', function (camera, $compile, AutoGen) {

    return {
        restrict: 'E',
        scope: {
            db_id_other: '@',
            cut: '=',
            selected: '@dataSelectedPosition',
            selectedChild: '@dataSelectedChild',
            selectedOption: '@',
            src_index: '@',
            src_index_other: '@',
            selActive: '@',
            activeType: '@'
        },

        templateUrl: 'templates/ctoria-parent.html',

        controller: function ($scope, $element, $attrs, AutoGen) {
            var childReadyCount = 0;

            $scope.selChosen = function(){
                return $scope.selActive;
            };

            $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                    if ($scope.cut.children[$scope.selectedChild-1].type == 'ALTERNATIVE_COMPOUND') {
                        $('#row_' + $scope.cut.index + '_sel').addClass('nextselectline');
                    }
                    $('#row_' + ($scope.cut.index + 1)).addClass('nextrowline');
                } else {
                    $('#icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                    if ($scope.cut.children[$scope.selectedChild-1].type == 'ALTERNATIVE_COMPOUND') {
                        $('#row_' + $scope.cut.index + '_sel').removeClass('nextselectline');
                    }
                    $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
                }
            };

            $scope.$on($scope.cut.index + '_childReady', function (e, val) {
                childReadyCount++;
                if (childReadyCount == $scope.cut.children.length) {
                    $scope.selectedChild = 1;//Math.floor((Math.random() * ($scope.cut.children.length - 1))) + 1;
                    var child_type = $scope.cut.children[$scope.selectedChild - 1].type;

                    var $row = $('#row_' + $scope.cut.index);
                    var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                    var $camera = $('#camera_' + $scope.cut.index);
                    var $selcamera = $('#selcamera_' + $scope.cut.index);
                    var $choicerow = $('#choicerow_' + $scope.cut.index);

                    switch (child_type) {
                        case 'ALTERNATIVE_COMPOUND':
                            $scope.activeType = 'COMPOUND';
                            //var $choicerow = $('#choicerow_' + $scope.cut.index);
                            var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                            var $sel = $("[data-cut-id=sel_" + $scope.cut.index + "]");
                            var $optSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + $scope.selectedChild + "]");

                            $scope.selectedOption = Number($optSel.attr('data-selected-position'));
                            $row.attr('data-ctoria-type', 'CHILD_COMPOUND');

                            $choicerow.on('mouseenter', rowEnter([$choicerow, $choice]));
                            $choicerow.on('mouseleave', rowLeave([$choicerow, $choice]));

                            $row.css('background-color', '#fbfdff');

                            $choice.removeClass('placementbtn').addClass('defaultbtn pull-left');
                            $optSel.css('color', '#ed6a43');
                            $def.css('color', '#ed6a43');
                            $choice.text($def.text());
                            $sel.text($optSel.text());
                            $choice.css('padding-left', '0px');

                            var defsources = $scope.cut.children[$scope.selectedChild - 1].default.sources;
                            var selsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;

                            $scope.src_index = setCamera($camera, defsources);
                            $scope.src_index_other =  setCamera($selcamera, selsources);
                            $scope.selActive = true;
                            $row.attr('data-file', defsources[$scope.src_index].file + ',' + selsources[$scope.src_index_other].file);

                            AutoGen($scope.cut.index);
                            break;

                        case "ALTERNATIVE_FREE":
                            $scope.activeType = 'FREE';
                            $scope.selActive = false;
                            var $selection = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild );
                            $scope.selected = Number($selection.attr('data-selected-position'));

                            var sources = $scope.cut.children[$scope.selectedChild-1].options[$scope.selected-1].sources;
                            $scope.src_index = setCamera($camera, sources);
                            $row.attr('data-file', sources[$scope.src_index].file);
                            $choicerow.on('mouseenter', rowEnter([$choicerow, $choice]));
                            $choicerow.on('mouseleave', rowLeave([$choicerow, $choice]));
                            var $child_choice = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild); //$scope.cut.children[$scope.selectedChild -1].options[$scope.selected].line;
                            $choice.text($child_choice.text());
                            $child_choice.css('color', '#ed6a43');

                            AutoGen($scope.cut.index);
                            break;

                        case 'ALTERNATIVE_DOUBLE':
                            $scope.activeType = 'DOUBLE';
                            var $choicerow = $('#choicerow_' + $scope.cut.index);
                            var $selrow = $('#selrow_' + $scope.cut.index);
                            var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                            var $sel = $("[data-cut-id=sel_" + $scope.cut.index + "]");
                            var $optSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + $scope.selectedChild + "]");

                            $scope.selectedOption = Number($optSel.attr('data-selected-position'));
                            $scope.selectedDefault = Number($def.attr('data-selected-position'));
                            var $defSel = $('#' + $scope.cut.index + '_child_' + $scope.selectedChild + '_def_'+ $scope.selectedDefault);
                            $row.attr('data-ctoria-type', 'CHILD_DOUBLE');
                            $choicerow.on('mouseenter', rowEnter([$choicerow, $choice]));
                            $choicerow.on('mouseleave', rowLeave([$choicerow, $choice]));
                            $row.css('background-color', '#fbfdff');
                            $choice.removeClass('placementbtn').addClass('defaultbtn pull-left');
                            $optSel.css('color', '#ed6a43');
                            $def.css('color', '#ed6a43');
                            $choice.text($defSel.text());
                            $sel.text($optSel.text());
                            $choice.css('padding-left', '0px');

                            var defsources = $scope.cut.children[$scope.selectedChild - 1].defaults[$scope.selectedDefault -1].sources;
                            var selsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                            $scope.src_index = setCamera($camera, defsources);
                            $scope.src_index_other =  setCamera($selcamera, selsources);
                            $scope.selActive = true;
                            $row.attr('data-file', defsources[$scope.src_index].file + ',' + selsources[$scope.src_index_other].file);

                            AutoGen($scope.cut.index);
                            break;

                        default:
                            console.log('default case in Parent switch case');
                            break;
                    }

                    $camera.on('click', function () {
                        var $row = $('#row_' + $scope.cut.index);
                        var $camera = $('#camera_' + $scope.cut.index);

                        switch($scope.activeType){
                            case 'COMPOUND':
                                var sources = $scope.cut.children[$scope.selectedChild - 1].default.sources;
                                var datafiles = $row.attr('data-file').split(',');
                                $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                                datafiles[0] = sources[$scope.src_index].file;
                                $row.attr('data-file', datafiles.toString());
                                break;
                            case 'FREE':
                                var freeSources = $scope.cut.children[$scope.selectedChild-1].options[$scope.selected-1].sources;
                                $scope.src_index = cameraToggle(freeSources, $scope.src_index, $camera, $row);
                                $row.attr('data-file',freeSources[$scope.src_index].file);
                                break;
                            case 'DOUBLE':
                                var doubleSources = $scope.cut.children[$scope.selectedChild - 1].defaults[$scope.selectedDefault-1].sources;
                                var doubleDatafiles = $row.attr('data-file').split(',');
                                $scope.src_index = cameraToggle(doubleSources, $scope.src_index, $camera, $row);
                                doubleDatafiles[0] = doubleSources[$scope.src_index].file;
                                $row.attr('data-file', doubleDatafiles.toString());
                                break;
                        }

                        $scope.$root.$broadcast('incrementCameraChanges');
                    });

                    $selcamera.on('click', function () {
                        var $selrow = $('#selrow_' + $scope.cut.index);

                        switch($scope.activeType) {
                            case 'DOUBLE':
                            case 'COMPOUND':
                                var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                                var datafiles = $row.attr('data-file').split(',');
                                $scope.src_index_other = cameraToggle(sources, $scope.src_index_other, $selcamera, $selrow);
                                datafiles[1] = sources[$scope.src_index_other].file;
                                $row.attr('data-file', datafiles.toString());
                                $scope.$root.$broadcast('incrementCameraChanges');
                                break;
                        }
                    });

                    childReadyCount = 0;
                }
            });

            $scope.$on($scope.cut.index + '_childSelected', function (e, senderType, val) {
                $scope.activeType = senderType;
                var $row = $('#row_' + $scope.cut.index);
                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");

                switch (senderType){
                    case 'FREE':
                        var $old_option = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var oldSelectedChild = $scope.selectedChild;
                        $scope.selected = val.selected;//index begins at 1  //$choice.attr('data-selected-position');
                        $scope.selectedChild = val.child_index;
                        var sources = $scope.cut.children[$scope.selectedChild-1].options[$scope.selected-1].sources;
                        $row.attr('data-file', sources[$scope.src_index].file);
                        var choice_txt = $scope.cut.children[$scope.selectedChild -1].options[$scope.selected-1].line;
                        var $option = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        $choice.text(choice_txt);
                        $option.css('color', '#ed6a43');
                        if(oldSelectedChild != $scope.selectedChild){
                            $old_option.css('color', '#333');
                        }

                        $scope.$root.$broadcast('incrementDialogueChanges');
                        $scope.fold($scope.cut.index);
                        break;
                    case 'COMPOUND':
                        var $def_row = $('#row_' + $scope.cut.index);
                        var $old_def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var $old_choice = $("[data-cut-id=" + $scope.cut.index + '_child_' + $scope.selectedChild + "]");
                        $old_def.css('color', '#333');
                        $old_choice.css('color', '#333');
                        var datafiles = $def_row.attr('data-file').split(',');
                        $scope.selectedChild = val;

                        var $new_def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var $new_choice = $("[data-cut-id=" + $scope.cut.index + '_child_' + $scope.selectedChild + "]");
                        $new_def.css('color', '#ed6a43');
                        $new_choice.css('color', '#ed6a43');
                        console.log($new_def.text());
                        $choice.text($new_def.text());
                        $scope.$root.$broadcast('incrementDialogueChanges');
                        var $sel = $("[data-cut-id=sel_" + $scope.cut.index + "]");
                        $sel.text($new_choice.text());
                        $scope.$root.$broadcast('incrementDialogueChanges');

                        var def_sources = $scope.cut.children[$scope.selectedChild - 1].default.sources;
                        datafiles[0] = def_sources[$scope.src_index].file;
                        $scope.selectedOption = Number($new_choice.attr('data-selected-position'));
                        var sel_sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                        datafiles[1] = sel_sources[$scope.src_index_other].file;
                        $def_row.attr('data-file', datafiles.toString());
                        $scope.fold($scope.cut.index);
                        break;
                    case 'DOUBLE':
                        var $def_row = $('#row_' + $scope.cut.index);
                        var $old_def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var $old_choice = $("[data-cut-id=" + $scope.cut.index + '_child_' + $scope.selectedChild + "]");
                        $old_def.css('color', '#333');
                        $old_choice.css('color', '#333');
                        var datafiles = $def_row.attr('data-file').split(',');
                        $scope.selectedChild = val;
                        var $new_def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var pos_def = Number($new_def.attr('data-selected-position'));
                        var $new_choice = $("[data-cut-id=" + $scope.cut.index + '_child_' + $scope.selectedChild + "]");
                        $new_def.css('color', '#ed6a43');
                        $new_choice.css('color', '#ed6a43');
                        var $def = $("[data-cut-id=" + $scope.cut.index + "]");
                        $def.text($new_def.text());
                        //$scope.$root.$broadcast('incrementDialogueChanges');
                        var $sel = $("[data-cut-id=sel_" + $scope.cut.index + "]");
                        $sel.text($new_choice.text());
                        $scope.$root.$broadcast('incrementDialogueChanges');

                        var def_sources = $scope.cut.children[$scope.selectedChild - 1].defaults[pos_def-1].sources;
                        datafiles[0] = def_sources[$scope.src_index].file;
                        $scope.selectedOption = Number($new_choice.attr('data-selected-position'));
                        var sel_sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                        //error here
                        datafiles[1] = sel_sources[$scope.src_index_other].file;
                        $def_row.attr('data-file', datafiles.toString());
                        $scope.fold($scope.cut.index);
                        break;
                }
            });

            var $selcamera = $('#selcamera_' + $scope.cut.index);
            $selcamera.on('click', function () {
                var $selrow = $('#selrow_' + $scope.cut.index);

                switch($scope.activeType) {
                    case 'DOUBLE':
                    case 'COMPOUND':
                        var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                        var datafiles = $row.attr('data-file').split(',');
                        $scope.src_index_other = cameraToggle(sources, $scope.src_index_other, $selcamera, $selrow);
                        datafiles[1] = sources[$scope.src_index_other].file;
                        $row.attr('data-file', datafiles.toString());
                        $scope.$root.$broadcast('incrementCameraChanges');
                        break;
                }
            });
        },

        link: function (scope, $element, $attr) {
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaChildFree', ['cameraFilter', 'speakerFilter', function(camera,speaker,$rootScope){
    return{
        restrict: 'E',
        scope: {
            selected: '@dataSelectedPosition',
            cut: '=',
            index: '='
        },

        templateUrl: 'templates/ctoria-child-free.html',

        controller: function($scope, $element, $rootScope){

            $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#alt_icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#alt_icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                } else {
                    $('#alt_icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                }
            };

            $scope.optionClick = function (val) {
                var $option = $('#' + val);
                var opt_txt = $option.text();
                var $choice = $('#alt_' + $scope.index + '_child_' + $scope.cut.position);

                $choice.text(opt_txt);

                $('#collapse_' + $scope.index + '_child_' + $scope.cut.position).collapse('toggle');
                $('#alt_icon_' + $scope.index + '_child_' + $scope.cut.position + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                console.log($scope.selected);
                $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected).prop("disabled", false).toggleClass('optionbtn-disabled');
                $scope.selected = Number($option.attr('data-position'));
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.attr('data-selected-position', $option.attr('data-position'));

                if($scope.$parent.cut.type == 'ALTERNATIVE_PARENT'){
                    $rootScope.$broadcast($scope.index + '_childSelected', 'FREE', {selected: $scope.selected, child_index: $scope.cut.position});
                }else{
                    $rootScope.$broadcast($scope.index + '_childSelected', {selected: $scope.selected, child_index: $scope.cut.position});
                }



            };

            $element.ready(function(){

                $scope.selected = Math.floor((Math.random() * $scope.cut.options.length) + 1);
                var $choice = $('#alt_' + $scope.index + '_child_' + $scope.cut.position );
                $choice.attr('data-selected-position', $scope.selected);

                var $choice_icon = $('#alt_icon_' + $scope.index + '_child_' + $scope.cut.position );
                var sources = $scope.cut.options[$scope.selected-1].sources;
                var $camera = $('#camera_' + $scope.index);
                $scope.src_index = setCamera($camera, sources);

                var $option = $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected);
                $choice.text($option.text());
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');

                $choice.on('mouseenter', rowEnter([$choice, $choice_icon]));
                $choice.on('mouseleave', rowLeave([$choice, $choice_icon]));

                $rootScope.$broadcast($scope.index + '_childReady','FREE');
            });
        },

        link: function($element){}
    }
}]);

scriptBoxWidgets.directive('ctoriaChildCompound', ['cameraFilter', 'speakerFilter', function (camera, speaker, $rootScope) {
    return {
        restrict: 'E',
        scope: {
            selected: '@dataSelectedPosition',
            cameraText: '@dataCameraText',
            cut: '=',
            index: '='
        },

        templateUrl: 'templates/ctoria-child-compound.html',

        controller: function ($scope, $element, $document, $rootScope) {

            $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#def_icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#def_icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                } else {
                    $('#def_icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                }
            };

            $scope.optionClick = function (val, pos) {
                var $option = $('#' + val);
                console.log($option.text());
                var $choice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                $choice.text($option.text());

                $('#collapse_' + $scope.index + '_child_' + $scope.cut.position).collapse('toggle');
                $('#def_icon_' + $scope.index + '_child_' + $scope.cut.position + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected).prop("disabled", false).toggleClass('optionbtn-disabled');
                $scope.selected = Number($option.attr('data-position'));
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.attr('data-selected-position', $option.attr('data-position'));
                $choice.attr('data-selected-option', pos);

                if($scope.$parent.cut.nested){
                    switch($scope.$parent.cut.type){
                        case "WEEKDAY":
                            $rootScope.$broadcast($scope.index + '_childSelected', val, pos);
                            break;
                        case "ALTERNATIVE_PAIRED"://$scope.$parent.cut.options.length + 1, $scope.$parent.cut.arguments.pos
                            $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.$parent.cut.options.length + 1, $scope.$parent.cut.arguments.pos);//pos);
                            break;
                    }
                }else if($scope.$parent.cut.type == 'ALTERNATIVE_PAIRED_PARENT') {
                    $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.cut.position, $scope.$parent.cut.arguments.pos);
                }else if($scope.$parent.cut.type == 'ALTERNATIVE_NESTED'){
                    $rootScope.$broadcast($scope.index + '_pairedSelected', $scope.selected, $scope.$parent.cut.arguments.pos, $scope.cut.position);
                }else{
                    $rootScope.$broadcast($scope.index + '_childSelected', 'COMPOUND', $scope.cut.position);
                }
            };

            $element.ready(function () {
                $scope.selected = Math.floor((Math.random() * $scope.cut.options.length) + 1);
                var $choice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                $choice.attr('data-selected-position', $scope.selected);
                var def_sources = $scope.cut.default.sources;
                var $def_camera = $('#camera_' + $scope.index);
                $scope.src_index = setCamera($def_camera, def_sources);
                var $option = $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected);
                $choice.text($option.text());
                $choice.attr('data-selected-option',$option.attr('data-position'));
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                var $def_choice = $('#def_' + $scope.index + '_child_' + $scope.cut.position);
                var $def_icon = $('#def_icon_' + $scope.index + '_child_' + $scope.cut.position);
                $def_choice.on('mouseenter', rowEnter([$def_choice, $def_icon]));
                $def_choice.on('mouseleave', rowLeave([$def_choice, $def_icon]));

                $rootScope.$broadcast($scope.index + '_childReady');
            });
        },

        link: function (scope, $element, $attr) {}
    }
}]);

scriptBoxWidgets.directive('ctoriaChildDouble', ['cameraFilter', 'speakerFilter',
    function(camera, speaker,$rootScope) {
        return {
            restrict: 'E',
            scope: {
                cut: '=',
                index: '=',
                selectedDefault: '@',
                selectedOption: '@',
                src_index: '@',
                src_index_other: '@'
            },

            templateUrl: 'templates/ctoria-child-double.html',

            controller: function ($scope, $element, $rootScope) {

                $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                } else {
                    $('#icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                }
                $('#selcollapse_' + val).collapse('toggle');
            };

                $scope.defClick = function (val, pos) {
                    var $choice = $("#def_" + $scope.index + "_child_" + $scope.cut.position);
                    var $defOption = $('#' + val);
                    var $oldDefOption = $('#' + $scope.index + '_child_' + $scope.cut.position + '_def_' + $scope.selectedDefault);
                    $oldDefOption.prop("disabled", false).toggleClass('optionbtn-disabled');
                    $defOption.prop("disabled", true).toggleClass('optionbtn-disabled');
                    $scope.selectedDefault = pos;
                    $choice.attr('data-selected-position', $scope.selectedDefault);
                    $choice.text($defOption.text());

                    $rootScope.$broadcast($scope.index + '_childSelected', 'DOUBLE', $scope.cut.position);
                };

                $scope.optionClick = function (val, pos) {
                    var $selChoice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                    var $selOption = $('#sel_' + val);
                    var $oldSelOption = $('#sel_' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selectedOption);
                    $oldSelOption.prop("disabled", false).toggleClass('optionbtn-disabled');
                    $selOption.prop("disabled", true).toggleClass('optionbtn-disabled');
                    $scope.selectedOption = pos;
                    $selChoice.attr('data-selected-position', $scope.selectedOption);
                    $selChoice.text($selOption.text());

                    $rootScope.$broadcast($scope.index + '_childSelected', 'DOUBLE', $scope.cut.position);
                };

                $element.ready(function () {
                    $scope.selectedDefault = Math.floor((Math.random() * $scope.cut.defaults.length) + 1);
                    $scope.selectedOption = Math.floor((Math.random() * $scope.cut.options.length) + 1);

                    var $choice = $("#def_" + $scope.index + "_child_" + $scope.cut.position);
                    var $selChoice = $("[data-cut-id=" + $scope.index + '_child_' + $scope.cut.position + "]");
                    var $icon = $('#icon_' + $scope.index + "_child_" + $scope.cut.position);

                    $choice.attr('data-selected-position', $scope.selectedDefault);
                    var defsources = $scope.cut.defaults[$scope.selectedDefault -1].sources;
                    var $defcamera = $('#camera_' + $scope.index);
                    $scope.src_index = setCamera($defcamera, defsources);
                    var $defoption = $('#' + $scope.index + '_child_' + $scope.cut.position + '_def_' + $scope.selectedDefault);
                    $choice.text($defoption.text());
                    $choice.attr('data-selected-option',$defoption.attr('data-position'));
                    $defoption.prop("disabled", true).toggleClass('optionbtn-disabled');
                    var selsources = $scope.cut.options[$scope.selectedOption -1].sources;
                    var $selcamera = $('#selcamera_' + $scope.index);
                    var $seloption = $('#sel_' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selectedOption);
                    $scope.src_index_other = setCamera($selcamera, selsources);
                    $selChoice.text($seloption.text());
                    $selChoice.attr('data-selected-position',$seloption.attr('data-position'));
                    $seloption.prop("disabled", true).toggleClass('optionbtn-disabled');
                    $choice.on('mouseenter', rowEnter([$choice, $icon]));
                    $choice.on('mouseleave', rowLeave([$choice, $icon]));

                    $rootScope.$broadcast($scope.index + '_childReady', 'DOUBLE');
                })
            }
        }
    }
]);

scriptBoxWidgets.directive('ctoriaPairedParent', ['cameraFilter', 'speakerFilter', 'LinkToCut', 'PairColour',
    function (camera, speaker, LinkToCut,  PairColour, $rootScope) {

        var changeSelection = function($scope, data){
            var $row = $('#row_' + $scope.cut.index);
            var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
            var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
            var $camera = $('#camera_' + $scope.cut.index);
            var $old_optSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + $scope.selectedChild + "]");
            $old_optSel.css('color', '#333');
            var oldSelectedOption = Number($old_optSel.attr('data-selected-option'));
            var oldSelectedChild = $scope.selectedChild;
            $scope.selectedChild = data;
            var $optSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + $scope.selectedChild + "]");
            $scope.selectedOption = Number($optSel.attr('data-selected-option'));
            $optSel.css('color', '#ed6a43');

            if ($scope.cut.children[$scope.selectedChild - 1].type == 'ALTERNATIVE_COMPOUND') {
                var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                var $selcamera = $('#selcamera_' + $scope.cut.index);
                var $selrow = $('#selrow_' + $scope.cut.index);
                var $sel = $('[data-cut-id=sel_' + $scope.cut.index + ']');
                $sel.text($optSel.text());
                $choice.text($def.text());
                var defsourceFile = $scope.cut.children[$scope.selectedChild - 1].default.sources[$scope.src_index].file;
                var optsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                $scope.src_index_other = setCamera($selcamera, optsources);
                var datafiles = $row.attr('data-file').split(',');
                datafiles[0] = defsourceFile;
                datafiles[1] = optsources[$scope.src_index_other].file;
                $row.attr('data-file', datafiles.toString());
                if (!$scope.nestedActive) {
                    $def.css('color', '#ed6a43');
                    $("[data-cut-id=" + $scope.cut.index + '_child_' + oldSelectedChild + "]").css('color','#333');
                    $scope.nestedActive = true;
                }

                if($scope.menuOpen){
                    toggleCollapse($scope.cut.index, $scope);
                    $scope.menuOpen = false;
                }

                $choice.add($row).add($selrow).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
            }else{
                var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                $scope.src_index = setCamera($camera, sources);
                $row.attr('data-file', sources[$scope.src_index].file);
                $speaker.text(speaker($scope.cut.index));
                $choice.text($optSel.text());
                $('#' + $scope.cut.index + '_child_'+ $scope.selectedChild + '_opt_' + $scope.selectedOption).css('color', '#333').prop("disabled", true).toggleClass('optionbtn-disabled');
                $('#' + $scope.cut.index + '_child_'+ oldSelectedChild + '_opt_' + oldSelectedOption).css('color', '#ed6a43').prop("disabled", false).toggleClass('optionbtn-disabled');

                if ($scope.nestedActive){
                    $('#def_' + $scope.cut.index + '_child_' + oldSelectedChild).css('color','#333');
                    $('#sel_' + $scope.cut.index + '_child_' + oldSelectedChild).css('color','#333');
                    $scope.nestedActive = false;
                }

                if($scope.menuOpen){
                    toggleCollapse($scope.cut.index, $scope);
                    $scope.menuOpen = false;
                }

                $choice.add($row).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
            }

            $choice.removeClass('placementborder');
            $row.removeClass('placementborder');
            $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');

            $scope.$root.$broadcast('incrementDialogueChanges');
        };
    return {
        restrict: 'E',
        scope: {
            cut: '=',
            selectedChild: '@dataSelectedChild',
            selectedOption: '@dataSelectedOption',
            nestedActive: '@',
            src_index: '@',
            src_index_other: '@',
            childReadyCount: '@',
            linkColours: '@',
            initialised: '@',
            menuOpen: '@'
        },
        templateUrl: 'templates/ctoria-paired-parent.html',

        controller: function ($scope, $element, $attrs, AutoGen) {
            $scope.childReadyCount = 0;
            $scope.initialised = false;
            $scope.nestedActive = false;
            $scope.menuOpen = false;

            $scope.fold = function (val) {
                toggleCollapse(val, $scope);
                $scope.menuOpen = !$scope.menuOpen;
            };

            $scope.nestedChosen = function(){
                return $scope.nestedActive;
            };

            var processChildren = function(){
                var $row = $('#row_' + $scope.cut.index);
                var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                var $camera = $('#camera_' + $scope.cut.index);
                var $optSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + Number($scope.selectedChild) + "]");
                $scope.selectedOption = Number($optSel.attr('data-selected-option'));
                var $childOpt = $("#" + $scope.cut.index + "_child_" + Number($scope.selectedChild) + "_opt_" + $scope.selectedOption);
                $childOpt.css('color', '#333').prop("disabled", true).toggleClass('optionbtn-disabled');

                if($scope.cut.children[Number($scope.selectedChild) - 1].type =='ALTERNATIVE_COMPOUND'){
                    var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                    var $selcamera = $('#selcamera_' + $scope.cut.index);
                    var $sel = $('[data-cut-id=sel_' + $scope.cut.index + ']');
                    $sel.text($optSel.text());
                    $choice.text($def.text());
                    var defsources = $scope.cut.children[$scope.selectedChild - 1].default.sources;
                    $scope.src_index = setCamera($camera, defsources);
                    var optsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                    $scope.src_index_other = setCamera($selcamera, optsources);
                    var datafiles = $row.attr('data-file').split(',');
                    datafiles[0] = defsources[$scope.src_index].file;
                    datafiles[1] = optsources[$scope.src_index_other].file;
                    $row.attr('data-file', datafiles.toString());
                    $def.css('color','#ed6a43');
                    $scope.nestedActive = true;
                }else{
                   var sources = $scope.cut.children[Number($scope.selectedChild) - 1].options[$scope.selectedOption - 1].sources; // was $attrs.s..
                    $scope.src_index = setCamera($camera, sources);
                    $row.attr('data-file', sources[$scope.src_index].file);
                    $choice.text($optSel.text());
                }

                $choice.attr('data-selected-child',$scope.selectedChild);
                $choice.attr('data-selected-option', $scope.selectedOption);

                $optSel.css('color', '#ed6a43');
                $speaker.text(speaker($scope.cut.index));
                $scope.childReadyCount = 0;

                $camera.on('click', function () {
                    if($scope.nestedActive){
                        var $choicerow = $('#choicerow_' + $scope.cut.index);
                        var defsources = $scope.cut.children[$scope.selectedChild-1].default.sources;
                        var datafiles = $row.attr('data-file').split(',');
                        $scope.src_index = cameraToggle(defsources, $scope.src_index, $camera, $choicerow);
                        datafiles[0] = defsources[$scope.src_index].file;
                        $row.attr('data-file', datafiles.toString());
                    }else{
                        var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;// was $attrs.s
                        $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                    }

                    $scope.$root.$broadcast('incrementCameraChanges');
                });

                $selcamera = $('#selcamera_' + $scope.cut.index);
                $selcamera.on('click', function () {
                    var $selrow = $('#selrow_' + $scope.cut.index);
                    var optsources = $scope.cut.children[$scope.selectedChild-1].options[$scope.selectedOption - 1].sources;
                    var datafiles = $row.attr('data-file').split(',');
                    $scope.src_index_other = cameraToggle(optsources, $scope.src_index_other, $selcamera, $selrow);
                    datafiles[1] = optsources[$scope.src_index_other].file;
                    $row.attr('data-file', datafiles.toString());

                    $scope.$root.$broadcast('incrementCameraChanges');
                });
            };

            if ($scope.cut.arguments.pos < $scope.cut.arguments.total) {
                    $scope.next = LinkToCut($scope.cut.arguments.next);
                    $scope.$on($scope.next + '_pairedSelected', function (e, val) {
                        console.log('next in paired selected we hear');
                        var $row = $('#row_' + $scope.cut.index);
                        var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                        $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                    });

                $rootScope.$broadcast($scope.cut.index, $scope.selectedChild);
            }

            if ($scope.cut.arguments.pos > 1) {
                $scope.previous = LinkToCut($scope.cut.arguments.prev);
                $scope.$on($scope.previous, function (e, data) {
                    $scope.selectedChild = data;
                    $scope.initialised = true;
                    if ($scope.childReadyCount == $scope.cut.children.length) {
                        var $row = $('#row_' + $scope.cut.index);
                        var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                        processChildren();
                        $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                    }
                });

                $scope.$on($scope.previous + '_pairedSelected', function (e, data, senderPos) {
                    if(senderPos < $scope.cut.arguments.pos) {
                        changeSelection($scope, data);
                    }
                });
            }
                $scope.$on($scope.cut.index + '_pairedSelected', function (e, data, senderPos) {
                    if(senderPos == $scope.cut.arguments.pos) {
                        changeSelection($scope, data);
                    }
                });

            $scope.$on($scope.cut.index + '_childReady', function (e, val) {
                $scope.childReadyCount++;
                if (($scope.childReadyCount == $scope.cut.children.length) && ($scope.initialised)) {
                    processChildren();
                    AutoGen($scope.cut.index)
                }
            })
        },

        link: function (scope, $element, $attrs) {
            $element.ready(function () {
                var $choice = $("[data-cut-id=" + scope.cut.index + "]");
                var $row = $('#row_' + scope.cut.index);
                var $link = $('#' + scope.cut.index + '_link');
                scope.linkColours = PairColour.getColours(scope.cut.index, scope.cut.arguments.pos, scope.cut.arguments.total);
                $link.css('background-color', scope.linkColours.bg);
                $row.on('mouseenter', rowEnter([$row, $choice]));
                $row.on('mouseleave', rowLeave([$row, $choice]));
            });
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaPairedFree', ['cameraFilter', 'speakerFilter', 'LinkToCut', 'AutoGen', 'PairColour',
    function (camera, speaker, LinkToCut, AutoGen, PairColour) {
    var autoSelect = function(scope, isReset){
        var $row = $('#row_' + scope.cut.index);
        var $speaker = $("[speaker-cut-id=" + scope.cut.index + "]");
        var $choice = $("[data-cut-id=" + scope.cut.index + "]");
        var $camera = $('#camera_' + scope.cut.index);

        if(isReset){
            $('#' + scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
        }

        if (scope.cut.arguments.pos == 1) {
            scope.selected = Math.floor((Math.random() * scope.cut.options.length) + 1);
            //scope.selected = scope.cut.options.length;//debug
        }

        if(scope.cut.nested && (scope.selected == scope.cut.options.length +1)){
            //scope.nestedActive = true;
            switch(scope.cut.nested.type){
                case 'ALTERNATIVE_FREE':
                    scope.$on(scope.cut.index + '_childReady', function () {
                    var $optSel = $("[data-cut-id=" + scope.cut.index + "_child_1]");
                        scope.selectedOption = Number($optSel.attr('data-selected-option'));
                        var sources = scope.cut.nested.options[scope.selectedOption - 1].sources;
                        scope.src_index = setCamera($camera, sources);
                        $row.attr('data-file', sources[scope.src_index].file);
                        $choice.attr('data-selected-position', scope.selected);
                        var $childOpt = $("#" + scope.cut.index + "_child_1" + "_opt_" + scope.selectedOption);
                        $childOpt.css('color', '#333').prop("disabled", true).toggleClass('optionbtn-disabled');
                        $optSel.css('color', '#ed6a43');
                        $speaker.text(speaker(scope.cut.index));
                        $choice.text($optSel.text());
                    });
                    break;
                case 'ALTERNATIVE_COMPOUND':
                    scope.nestedActive = true;
                    break;
                default:
                    scope.nestedActive = true;

            }
        }else{
            scope.nestedActive = false;
            $choice.attr('data-selected-position', scope.selected);
            var sources = scope.cut.options[scope.selected - 1].sources;
            scope.src_index = setCamera($camera, sources);
            $row.attr('data-file', sources[scope.src_index].file);
            var $option = $('#' + scope.cut.index + '_opt_' + scope.selected);
            $choice.text($option.text());
            $option.prop("disabled", true).toggleClass('optionbtn-disabled');
        }

        scope.$root.$broadcast(scope.cut.index, scope.selected);
        AutoGen(scope.cut.index);
    };
    return {
        restrict: 'E',
        scope: {
            src_index: '@',
            src_index_other: '@',
            selected: '@dataSelectedPosition',
            otherSelected: '@',
            selectedOption: '@',
            linkColours: '@',
            cut: '=',
            nestedActive: '@',
            menuOpen: '@'
        },
        templateUrl: 'templates/ctoria-paired-free.html',
        controller: function ($scope, $element, $rootScope, $attrs) {
            $scope.menuOpen = false;

            $scope.fold = function (val) {
                toggleCollapse(val, $scope);
                $scope.menuOpen = !$scope.menuOpen;
            };

            $scope.$on('reset', function(){
               autoSelect($scope, true)
            });

            $scope.nestedChosen = function(){
                return $scope.nestedActive;
            };

            $scope.optionClick = function (val) {
                var $row = $('#row_' + $scope.cut.index);
                var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                var $option = $('#' + val);
                var opt_txt = $option.text();

                $speaker.text(speaker($scope.cut.index));
                $choice.text(opt_txt);
                $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                var sources = $scope.cut.options[$option.attr('data-position') - 1].sources;
                $row.attr('data-file', sources[$scope.src_index].file);
                $scope.$root.$broadcast('incrementDialogueChanges');

                $('#collapse_' + $scope.cut.index).collapse('toggle');
                $('#icon_' + $scope.cut.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                if ($choice.attr('data-selected-position') != 0) {
                    $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
                    $('#row_' + $scope.cut.index).removeClass('placementborder');
                    $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
                }
                $scope.menuOpen = false;

                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.attr('data-selected-position', $option.attr('data-position'));
                $scope.selected = $option.attr('data-position');
                if($scope.nestedActive){
                    $scope.nestedActive = false;
                    var $def = $("#def_" + $scope.cut.index + "_child_1");
                    var $selrow = $("#sel_" + $scope.cut.index + "_child_1");//'[data-cut-id=sel_' + $scope.cut.index + "]");
                    $def.css('color', '#333');
                    $selrow.css('color', '#333');
                }

                $scope.$root.$broadcast($scope.cut.index + '_pairedSelected', $scope.selected, $scope.cut.arguments.pos);
            };

            if ($scope.cut.arguments.pos > 1) {
                $scope.previous = LinkToCut($scope.cut.arguments.prev);
                $scope.$on($scope.previous, function (e, data) {
                    $scope.selected = data;
                    autoSelect($scope, false);
                });

                $scope.$on($scope.previous + '_pairedSelected', function (e, val, senderPos) {
                    if(senderPos  < $scope.cut.arguments.pos) {
                        var $row = $('#row_' + $scope.cut.index);
                        var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                        if(Number($choice.attr('data-selected-position')) == $scope.cut.options.length +1){
                            $('[data-cut-id=' + $scope.cut.index + '_child_1]').css('color', '#333');
                        }else{
                            $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
                        }

                        $scope.selected = Number(val);

                        if($scope.cut.nested && $scope.selected == $scope.cut.options.length +1){
                            if($scope.cut.nested.type == "ALTERNATIVE_FREE"){
                                var $nestedopt = $('[data-cut-id=' + $scope.cut.index + '_child_1]');
                                var selectedopt = Number($nestedopt.attr('data-selected-option'));
                                var nestedopt_txt = $nestedopt.text();
                                $choice.text(nestedopt_txt);
                                var optfile = $scope.cut.nested.options[selectedopt-1].sources[$scope.src_index].file;
                                $row.attr('data-file', optfile);
                            }else{
                                $scope.nestedActive = true;
                                console.log('sort it out in the paired free $scope.previous listener function for compounds')
                            }
                        }else{
                            var $option = $('#' + $scope.cut.index + '_opt_' + $scope.selected);
                            var new_opt_txt = $option.text();
                            var sources = $scope.cut.options[$scope.selected - 1].sources;
                            $row.attr('data-file', sources[$scope.src_index].file);
                            $choice.attr('data-selected-position', val);
                            var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                            $speaker.text(speaker($scope.cut.index));
                            $choice.text(new_opt_txt);
                        }

                        if($scope.menuOpen){
                            toggleCollapse($scope.cut.index, $scope);
                            $scope.menuOpen = false;
                        }


                        $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                        $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", true).toggleClass('optionbtn-disabled');
                        $scope.$root.$broadcast('incrementDialogueChanges');
                        $scope.$root.$broadcast($scope.cut.index + '_pairedSelected', $scope.selected, senderPos);
                    }
                })
            }
            
            if ($scope.cut.arguments.pos < $scope.cut.arguments.total) {
                $scope.next = LinkToCut($scope.cut.arguments.next);
                $scope.$on($scope.next + '_pairedSelected', function (e, val, senderPos) {
                    if(senderPos  > $scope.cut.arguments.pos) {
                        var $row = $('#row_' + $scope.cut.index);
                        var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                        if(Number($choice.attr('data-selected-position')) == $scope.cut.options.length +1){
                            $('[data-cut-id=' + $scope.cut.index + '_child_1]').css('color', '#333');
                        }else{
                            $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", false).toggleClass('optionbtn-disabled');
                        }
                        var $option = $('#' + $scope.cut.index + '_opt_' + val);
                        var new_opt_txt = $option.text();
                        $scope.selected = val;
                        var sources = $scope.cut.options[$scope.selected - 1].sources;
                        $row.attr('data-file', sources[$scope.src_index].file);
                        $choice.attr('data-selected-position', val);
                        var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                        $speaker.text(speaker($scope.cut.index));
                        $choice.text(new_opt_txt);
                        /*console.log($('#collapse_' + $scope.cut.index).collapse())*/
                        if($scope.menuOpen){
                            toggleCollapse($scope.cut.index, $scope);
                            $scope.menuOpen = false;
                        }

                        $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                        $('#' + $scope.cut.index + '_opt_' + $choice.attr('data-selected-position')).prop("disabled", true).toggleClass('optionbtn-disabled');
                        $scope.$root.$broadcast('incrementDialogueChanges');
                        $scope.$root.$broadcast($scope.cut.index + '_pairedSelected', $scope.selected, senderPos);
                    }
                });
            }

            if($scope.cut.nested){
                $scope.$on($scope.cut.index + '_pairedSelected', function(e, val, senderPos){
                    if(val == $scope.cut.options.length + 1 && senderPos == $scope.cut.arguments.pos) {
                        var $child_choice = $('[data-cut-id=' + $scope.cut.index + '_child_1]');
                        $scope.selectedOption = Number($child_choice.attr('data-selected-option'));
                        var $row = $('#row_' + $scope.cut.index);
                        var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                        var old_choice_num = Number($choice.attr('data-selected-position'));
                        if (old_choice_num <= $scope.cut.options.length) {
                            var $old_choice = $('#' + $scope.cut.index + '_opt_' + old_choice_num);
                            $old_choice.prop("disabled", false).toggleClass('optionbtn-disabled');
                        }
                        if($scope.cut.nested.type == 'ALTERNATIVE_COMPOUND'){
                            $choice.attr('data-selected-position', $scope.cut.options.length +1);
                            var $def = $("#def_" + $scope.cut.index + "_child_1");
                            $scope.otherSelected =  $child_choice.attr('data-selected-option');
                            var $sel = $('#'+$scope.cut.index + "_child_1_opt_" + $scope.otherSelected);//$child_choice.attr('data-selected-option'));//
                            var $selcamera = $('#selcamera_' + $scope.cut.index);
                            var $selrow = $('[data-cut-id=sel_' + $scope.cut.index + "]");
                            var defsourceFile = $scope.cut.nested.default.sources[$scope.src_index].file;
                            var selsources = $scope.cut.nested.options[$scope.otherSelected-1].sources;
                            var datafiles = $row.attr('data-file').split(',');
                            $scope.src_index_other = cameraToggle(selsources, $scope.src_index_other, $selcamera, $selrow);
                            datafiles[0] = defsourceFile;
                            datafiles[1] = selsources[$scope.src_index_other].file;
                            $row.attr('data-file', datafiles.toString());
                            $choice.text($def.text());
                            $selrow.text($sel.text());
                            $def.css('color', '#ed6a43');
                            $scope.nestedActive = true;
                        }else if($scope.cut.nested.type == 'ALTERNATIVE_FREE'){
                            $choice.attr('data-selected-position', $scope.cut.options.length +1);
                            var sources = $scope.cut.nested.options[$scope.selectedOption - 1].sources;
                            $row.attr('data-file', sources[$scope.src_index].file);
                            var $optSel = $('[data-cut-id=' + $scope.cut.index + '_child_1]')
                            $choice.text($optSel.text());
                            $scope.nestedActive = false;
                        }

                        $scope.selected = val;
                        $choice.attr('data-selected-position', $scope.selected);
                        $child_choice.css('color', '#ed6a43');
                        if($scope.menuOpen){
                            toggleCollapse($scope.cut.index, $scope);
                            $scope.menuOpen = !$scope.menuOpen;
                        }
                        //$('#collapse_' + $scope.cut.index).collapse('toggle');
                        $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                    }
                });
            }

            $element.ready(function () {
                var $row = $('#row_' + $scope.cut.index);
                var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                var $camera = $('#camera_' + $scope.cut.index);
                var $link = $('#' + $scope.cut.index + '_link');
                $scope.linkColours = PairColour.getColours($scope.cut.index, $scope.cut.arguments.pos, $scope.cut.arguments.total);
                $link.css('background-color', $scope.linkColours.bg);
                console.log('pos: ' + $scope.cut.arguments.pos + ' index: ' + $scope.cut.index);
                if ($scope.cut.arguments.pos == 1) {
                    autoSelect($scope, false);
                }

                $speaker.text(speaker($scope.cut.index));

                $row.on('mouseenter', rowEnter([$row, $choice]));
                $row.on('mouseleave', rowLeave([$row, $choice]));

                $camera.on('click', function () {
                    var $row = $('#row_' + $scope.cut.index);
                    var sources;
                    if($scope.selected == $scope.cut.options.length + 1){
                        if($scope.cut.nested.type == "ALTERNATIVE_FREE"){
                            sources = $scope.cut.nested.options[$scope.selectedOption - 1].sources;
                        }else if($scope.cut.nested.type == "ALTERNATIVE_COMPOUND"){
                            var $choicerow = $('#choicerow_' + $scope.cut.index);
                            var defsources = $scope.cut.nested.default.sources;
                            var datafiles = $row.attr('data-file').split(',');
                            $scope.src_index = cameraToggle(defsources, $scope.src_index, $camera, $choicerow);
                            datafiles[0] = defsources[$scope.src_index].file;
                            $row.attr('data-file', datafiles.toString());
                        }
                    }else{
                        sources = $scope.cut.options[$scope.selected - 1].sources;
                        $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                    }

                    $scope.$root.$broadcast('incrementCameraChanges');
                });

                if($scope.cut.nested){
                    $selcamera = $('#selcamera_' + $scope.cut.index);
                    $selcamera.on('click', function () {
                        var $selrow = $('#selrow_' + $scope.cut.index);
                        var sources = $scope.cut.nested.options[$scope.otherSelected - 1].sources;
                        var datafiles = $row.attr('data-file').split(',');
                        $scope.src_index_other = cameraToggle(sources, $scope.src_index_other, $selcamera, $selrow);
                        datafiles[1] = sources[$scope.src_index_other].file;
                        $row.attr('data-file', datafiles.toString());

                        $scope.$root.$broadcast('incrementCameraChanges');
                    });
                }

                //AutoGen($scope.cut.index)
            });
        },

        link: function (scope, $element, $attr, $scope) {
        }
    }
}]);

scriptBoxWidgets.directive('ctoriaControl', ['Play', '$controller', '$location', '$sce', function (Play, $controller, $location, $sce) {
    return {
        restrict: 'E',
        scope: {
            title: '@',
            generated: '@',
            selected: '@',
            first: '@',
            last: '@',
            user: '@',
            selectedMenuItem: '@',
            menuOpen: '@',//,
            autoURL: '@'
        },

        templateUrl: 'templates/ctoria-control.html',

        controller: function ($scope, $element, $location, $sce) {
            $scope.dialogueChanges = 0;
            $scope.cameraChanges = 0;
            $scope.length = 0;
            $scope.user = 'Anonymous user';
            $scope.selected = "none";
            $scope.menuOpen = false;
            $scope.autoURL = $sce.trustAsResourceUrl("http://172.17.0.2/#/auto");//http://localhost:8000/auto
            //http://localhost:8000/templates/autoscript.htm


            $scope.showConversations = function(){
                if($location.path() == '/jane'){
                    return true;
                }else{
                    return false;
                }
            };

            $scope.$on('changeConversation', function(e, data){
                console.log('change conversation heard in ControlDirective  --changeConversation: $scope.selected: ' + $scope.selected + ' data: ' + data)
                $('#' + $scope.selected).css('color', '#333');
                $scope.selected = data;
                //var item = $('#' + code);
                $('#' + data).css('color', '#ed6a43');
            });

            $scope.$on('conversationGenerated', function(e, data){
                console.log('change conversation heard in ControlDirective: --conversationGenerated: $scope.selected: ' + $scope.selected + ' data: ' + data)
                $('#' + $scope.selected).css('color', '#333');
                $scope.selected = data;
                $('#' + data).css('color', '#ed6a43');
                $scope.cameraChanges = 0;
                $scope.dialogueChanges = 0;
            });



            $element.ready(function(){
                $scope.selectedMenuItem = $location.path().slice(1);
                var $menuItem = $('#' + $scope.selectedMenuItem + '> a');
                $menuItem.addClass('nav-active');
                $menuItem.css('color', '#a71d5d');
            });

            $scope.dropdownActive = function(id){
                $('#'+$scope.selectedMenuItem).removeClass('active');
                var $oldMenuItem = $('#' + $scope.selectedMenuItem + '> a');
                $oldMenuItem.removeClass('nav-active');
                $oldMenuItem.css('color', '#183691');
            };

            $scope.changeGoPage = function(page, parentId){
                var $pageBtn =$('#' + page.slice(1));
                var $dropdown = $('#'+ parentId);
                $dropdown.text($pageBtn.text());
                var view =  $("#ngView");
                view.addClass('full');
                $location.path(page);
            };

            $scope.goPage = function(page){
                $scope.selectedMenuItem = page.slice(1);
                var view =  $("#ngView");
                view.addClass('full');
                $location.path(page);

            };

            $scope.goJane = function(page){

                var video = document.createElement('video');
                var canPlay = false;
                if(video.canPlayType){
                   if(video.canPlayType('video/mp4') == 'maybe' || video.canPlayType('video/mp4') == 'probably')
                   {
                       $scope.selectedMenuItem = page.slice(1);
                       var view =  $("#ngView");
                       view.addClass('full');
                       canPlay = true;
                       $location.path(page);
                   }
                }

                if(!canPlay){
                   var view =  $("#ngView");
                    view.addClass('full');
                    $location.path(page);
                }

            }

            $scope.$on('incrementDialogueChanges', function () {
                $scope.dialogueChanges++;
            });

            $scope.$on('conversationGenerated', function(e, code){
                if($scope.selected == "none"){
                    $scope.selected = code;
                    $('#' + code).css('color', '#ed6a43');
                }
            });

            $scope.$on('incrementCameraChanges', function (e, apply) {
                $scope.cameraChanges++;
                if(apply==undefined)$scope.$apply();
            });
        },
        link: function ($scope, $element, $location) {
            $element.ready(function () {
                var $conversationMenu = $('#conversationMenu');
                var $mainMenu = $('#mainMenuButton');

                $mainMenu.on('click', function(){
                    /*$('#mainMenu').toggle();*/
                    $scope.menuOpen = !$scope.menuOpen;
                    if($scope.menuOpen){
                        $mainMenu.addClass('fa-rotate-90')
                    }else{
                        $mainMenu.removeClass('fa-rotate-90')
                    }
                    $mainMenu.css('color', '#ff0000');
                });

                $scope.changeConversation = function(code){
                    console.log('change conversation code: ' + code)
                    $('#' + $scope.selected).css('color', '#333');
                    $scope.selected = code;
                    var item = $('#' + code);
                    item.css('color', '#ed6a43');
                    /*$('#menuConversation').toggle();*/

                    $scope.$root.$broadcast('changeConversation', code)
                };
            })
        }
    }
}]);


scriptBoxWidgets.directive('ctoriaPairedNested', ['cameraFilter', 'speakerFilter', 'LinkToCut', 'PairColour',
    function (camera, speaker, LinkToCut,  PairColour, $rootScope) {

        var changeSelection = function($scope, opt, child, fromSelf){
            var $row = $('#row_' + $scope.cut.index);
            var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
            var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
            var $camera = $('#camera_' + $scope.cut.index);
            var oldSelectedChild = $scope.selectedChild;
            var oldSelectedOption = $scope.selectedOption;
            var otherChild;//, $newSel;

            if(fromSelf){
                otherChild = child==1?2:1;
                $scope.selectedChild = child;
                $scope.selectedOption = opt; //Number($newSel.attr('data-selected-position'));
            }else{
                otherChild = $scope.selectedChild==1?2:1;
                $scope.selectedOption = opt;
            }

            var $newSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + $scope.selectedChild + "]");
            var $otherSel = $("[data-cut-id=" + $scope.cut.index + "_child_" + otherChild + "]");
            var $otherSelNewOpt = $("#" + $scope.cut.index + "_child_" + otherChild + '_opt_' + $scope.selectedOption);

            if($scope.symmetric){//in symmetic we only once have two compounds
                var $otherSelOldOpt = $('#' + $scope.cut.index + "_child_" + otherChild + "_opt_" + oldSelectedOption);
                var $selOldOpt = $('#' + $scope.cut.index + "_child_" + $scope.selectedChild + "_opt_" + oldSelectedOption);
                var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);

                if($scope.selectedChild != oldSelectedChild){
                    var $oldDef = $('#def_' + $scope.cut.index + "_child_" + otherChild);
                    $oldDef.css('color', '#333');
                    $otherSel.css('color', '#333');
                    $def.css('color', '#ed6a43');
                    $newSel.css('color', '#ed6a43');
                    $selOldOpt.css('color', '#333');
                    $otherSelOldOpt.prop("disabled", false).toggleClass('optionbtn-disabled');
                    $otherSelNewOpt.css('color', '#ed6a43');

                }else{ //same child selected
                    if(!fromSelf){//you need to change selection of chosen child
                        var $selNewOpt = $("#" + $scope.cut.index + "_child_" + $scope.selectedChild + '_opt_' + $scope.selectedOption);
                        $newSel.text($selNewOpt.text());
                        $selNewOpt.prop("disabled", true).toggleClass('optionbtn-disabled');
                        $newSel.attr('data-selected-position', $scope.selectedOption);
                    }
                    $otherSelOldOpt.css('color', '#333');
                    $otherSelNewOpt.css('color', '#ed6a43');//prop("disabled", true).toggleClass('optionbtn-disabled');
                    $selOldOpt.prop("disabled", false).toggleClass('optionbtn-disabled');
                }

                $otherSel.attr('data-selected-position', $scope.selectedOption);
                $otherSel.text($otherSelNewOpt.text());

                var $selcamera = $('#selcamera_' + $scope.cut.index);
                var $selrow = $('#selrow_' + $scope.cut.index);
                var $sel = $('[data-cut-id=sel_' + $scope.cut.index + ']');
                $sel.text($newSel.text());
                $choice.text($def.text());
                var defsourceFile = $scope.cut.children[$scope.selectedChild - 1].default.sources[$scope.src_index].file;
                var optsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                $scope.src_index_other = setCamera($selcamera, optsources);
                var datafiles = $row.attr('data-file').split(',');
                datafiles[0] = defsourceFile;
                datafiles[1] = optsources[$scope.src_index_other].file;
                $row.attr('data-file', datafiles.toString());
                $choice.add($row).add($selrow).add($sel).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);

            }else{ //i.e asymmetric

                if(!fromSelf && $scope.selectedChild!=$scope.target){
                    return;
                }

                if($scope.selectedChild != oldSelectedChild){
                    $otherSel.css('color', '#333');
                    $newSel.css('color', '#ed6a43');
                }

                if($scope.selectedChild == $scope.target) {
                    var $targetOldOpt = $('#' + $scope.cut.index + "_child_" + $scope.selectedChild + "_opt_" + oldSelectedOption);
                    $targetOldOpt.prop("disabled", false).toggleClass('optionbtn-disabled');
                    if(!fromSelf){
                       var $targetNewOpt = $("#" + $scope.cut.index + "_child_" + $scope.selectedChild + '_opt_' + $scope.selectedOption);
                        $newSel.text($targetNewOpt.text());
                        $targetNewOpt.prop("disabled", true).toggleClass('optionbtn-disabled');
                        $newSel.attr('data-selected-position', $scope.selectedOption);
                    }
                }

                var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                $scope.src_index = setCamera($camera, sources);
                $row.attr('data-file', sources[$scope.src_index].file);
                $speaker.text(speaker($scope.cut.index));
                $choice.text($newSel.text());
                $choice.add($row).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
            }

            if($scope.menuOpen){
                toggleCollapse($scope.cut.index, $scope);
                $scope.menuOpen = false;
            }

            $choice.removeClass('placementborder');
            $row.removeClass('placementborder');
            $('#row_' + ($scope.cut.index + 1)).removeClass('nextrowline');
            /*if(fromSelf){
                $('#collapse_' + $scope.cut.index).collapse("hide");
            }
            if ($('#icon_' + $scope.cut.index + '> i').hasClass('fa-chevron-down')){
                $('#icon_' + $scope.cut.index + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
            }*/

            $scope.$root.$broadcast('incrementDialogueChanges');
        };
        return {
            restrict: 'E',
            scope: {
                cut: '=',
                selectedChild: '@dataSelectedChild',
                selectedOption: '@dataSelectedOption',
                nestedActive: '@',
                src_index: '@',
                src_index_other: '@',
                childReadyCount: '@',
                linkColours: '@',
                initialised: '@',
                symmetric : '@',
                target :'@',
                menuOpen: '@'
            },
            templateUrl: 'templates/ctoria-paired-parent.html',

            controller: function ($scope, $element, $attrs, AutoGen) {
                $scope.childReadyCount = 0;
                $scope.initialised = false;
                $scope.menuOpen = false;

                if($scope.cut.style == 'symmetric'){
                    $scope.symmetric = true;
                    $scope.nestedActive = true;
                }else{
                    $scope.symmetric = false;
                    $scope.nestedActive = false;
                    $scope.target = Number($scope.cut.target);
                }

                $scope.fold = function (val) {
                    toggleCollapse(val, $scope);
                    $scope.menuOpen = !$scope.menuOpen;
                };

                $scope.nestedChosen = function(){
                    return $scope.nestedActive;
                };

                var processChildren = function(){
                    var $row = $('#row_' + $scope.cut.index);
                    var $speaker = $("[speaker-cut-id=" + $scope.cut.index + "]");
                    var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                    var $camera = $('#camera_' + $scope.cut.index);
                    var $selcamera = $('#selcamera_' + $scope.cut.index);
                    $scope.selectedChild = Math.floor((Math.random() * $scope.cut.children.length)) + 1;

                    if($scope.symmetric){
                        for(var a=0; a < $scope.cut.children.length; a++){
                            var $selection = $('#sel_' + $scope.cut.index + '_child_' + (a+1));
                            var selectedOpt = $selection.attr('data-selected-position');
                            var $childOpt = $('#' + $scope.cut.index + '_child_' + (a+1) + '_opt_' + selectedOpt);
                            $childOpt.prop("disabled", false).toggleClass('optionbtn-disabled');
                            var $newSel = $('#' + $scope.cut.index + '_child_' + (a+1) + '_opt_' + $scope.selectedOption);
                            if((a+1) == $scope.selectedChild) {
                                $newSel.prop("disabled", true).toggleClass('optionbtn-disabled');
                            }else{
                                $newSel.css('color', '#ed6a43' )
                            }
                            $selection.text($newSel.text());
                            $selection.attr('data-selected-position', $scope.selectedOption)
                        }

                        var $def = $('#def_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var $sel = $('#sel_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                        var $selChoice = $('[data-cut-id=sel_' + $scope.cut.index + ']');
                        $choice.text($def.text());
                        $selChoice.text($sel.text());
                        $def.css('color','#ed6a43');
                        $sel.css('color','#ed6a43');
                        var defsources = $scope.cut.children[$scope.selectedChild - 1].default.sources;
                        $scope.src_index = setCamera($camera, defsources);
                        var optsources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;
                        $scope.src_index_other = setCamera($selcamera, optsources);
                        var datafiles = $row.attr('data-file').split(',');
                        datafiles[0] = defsources[$scope.src_index].file;
                        datafiles[1] = optsources[$scope.src_index_other].file;
                        $row.attr('data-file', datafiles.toString());
                        $scope.nestedActive = true;

                        $scope.$root.$broadcast($scope.cut.index, $scope.selectedOption);

                    }else{ // asymmetric

                        for(var b =0; b < $scope.cut.children.length; b++) {
                            if((b+1) == $scope.target){
                                var $select = $('[data-cut-id=' + $scope.cut.index + '_child_' + $scope.target + ']');
                                var selectedOption = $select.attr('data-selected-option');
                                var $oldSelect = $('#' + $scope.cut.index + '_child_' + $scope.target + '_opt_' + selectedOption);
                                $oldSelect.prop("disabled", false).toggleClass('optionbtn-disabled');
                                $oldSelect.css('color', '#333');
                                var $newSelect = $('#' + $scope.cut.index + '_child_' + $scope.target + '_opt_' + $scope.selectedOption);
                                $newSelect.prop("disabled", true).toggleClass('optionbtn-disabled');
                                var seltxt = $newSelect.text();
                                $select.text($newSelect.text());
                                $select.attr('data-selected-position', $scope.selectedOption);
                            }
                        }

                        if($scope.selectedChild == $scope.target){
                            $select.css('color', '#ed6a43');
                            $choice.text($select.text());
                            $scope.$root.$broadcast($scope.cut.index, $scope.selectedOption);
                        }else{
                            var $optSel = $('[data-cut-id='+ $scope.cut.index + '_child_' + $scope.selectedChild + ']');
                            $scope.selectedOption = Number($optSel.attr('data-selected-option'));
                            $optSel.css('color', '#ed6a43');
                            $choice.text($optSel.text());
                        }
                        var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption-1].sources;
                        $scope.src_index = setCamera($camera, sources);
                        $row.attr('data-file', sources[$scope.src_index].file);
                        $scope.nestedActive = false;
                    }

                    $choice.attr('data-selected-child',$scope.selectedChild);
                    $choice.attr('data-selected-option', $scope.selectedOption);

                    $speaker.text(speaker($scope.cut.index));
                    $scope.childReadyCount = 0;

                    $camera.on('click', function () {
                        if($scope.nestedActive){
                            var $choicerow = $('#choicerow_' + $scope.cut.index);
                            var defsources = $scope.cut.children[$scope.selectedChild-1].default.sources;
                            var datafiles = $row.attr('data-file').split(',');
                            $scope.src_index = cameraToggle(defsources, $scope.src_index, $camera, $choicerow);
                            datafiles[0] = defsources[$scope.src_index].file;
                            $row.attr('data-file', datafiles.toString());
                        }else{
                            var sources = $scope.cut.children[$scope.selectedChild - 1].options[$scope.selectedOption - 1].sources;// was $attrs.s
                            $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                        }

                        $scope.$root.$broadcast('incrementCameraChanges');
                    });

                    $selcamera = $('#selcamera_' + $scope.cut.index);
                    $selcamera.on('click', function () {
                        var $selrow = $('#selrow_' + $scope.cut.index);
                        var optsources = $scope.cut.children[$scope.selectedChild-1].options[$scope.selectedOption - 1].sources;
                        var datafiles = $row.attr('data-file').split(',');
                        $scope.src_index_other = cameraToggle(optsources, $scope.src_index_other, $selcamera, $selrow);
                        datafiles[1] = optsources[$scope.src_index_other].file;
                        $row.attr('data-file', datafiles.toString());

                        $scope.$root.$broadcast('incrementCameraChanges');
                    });
                };

                if ($scope.cut.arguments.pos < $scope.cut.arguments.total) {
                    $scope.next = LinkToCut($scope.cut.arguments.next);
                    $scope.$on($scope.next + '_pairedSelected', function (e, val, senderPos, child) {
                        if(senderPos  > $scope.cut.arguments.pos && child==1) { //hacky since we know child 1 is paired
                            changeSelection($scope, val, null, false);
                            $scope.$root.$broadcast($scope.cut.index + '_pairedSelected', $scope.selectedOption, senderPos);
                        }
                    });
                }

                if ($scope.cut.arguments.pos > 1) {
                    $scope.previous = LinkToCut($scope.cut.arguments.prev);
                    $scope.$on($scope.previous, function (e, data) {
                        $scope.selectedOption = data;
                        $scope.initialised = true;
                        if ($scope.childReadyCount == $scope.cut.children.length) {
                            var $row = $('#row_' + $scope.cut.index);
                            var $choice = $("[data-cut-id=" + $scope.cut.index + "]");
                            processChildren();
                            $row.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                        }
                    });

                    $scope.$on($scope.previous + '_pairedSelected', function (e, data, senderPos, child) {
                        if(senderPos < $scope.cut.arguments.pos) {
                            changeSelection($scope, data, child, false);
                            if($scope.cut.arguments.pos < $scope.cut.arguments.total){
                                $scope.$root.$broadcast($scope.cut.index + '_pairedSelected', $scope.selectedOption, senderPos);
                            }
                        }
                    });
                }

                $scope.$on($scope.cut.index + '_pairedSelected', function (e, selected, senderPos, child) {
                    if(senderPos == $scope.cut.arguments.pos) {
                        changeSelection($scope, selected, child, true);
                    }
                });

                $scope.$on($scope.cut.index + '_childReady', function (e, val) {
                    $scope.childReadyCount++;
                    if (($scope.childReadyCount == $scope.cut.children.length) && ($scope.initialised)) {
                        processChildren();
                        AutoGen($scope.cut.index)
                    }
                })
            },

            link: function (scope, $element, $attrs) {
                $element.ready(function () {
                    var $choice = $("[data-cut-id=" + scope.cut.index + "]");
                    var $row = $('#row_' + scope.cut.index);
                    var $link = $('#' + scope.cut.index + '_link');
                    scope.linkColours = PairColour.getColours(scope.cut.index, scope.cut.arguments.pos, scope.cut.arguments.total);
                    $link.css('background-color', scope.linkColours.bg);
                    $row.on('mouseenter', rowEnter([$row, $choice]));
                    $row.on('mouseleave', rowLeave([$row, $choice]));
                });
            }
        }
    }]);

    scriptBoxWidgets.directive('ctoriaLoop', ['cameraFilter', 'speakerFilter', 'AutoGen',
        function (camera, speaker, AutoGen) {
            var selectOptions = function($scope, oddOrEven, selectAmount ){
                var selections = [];
                var opts = [1,2,3,4,5,6,7,8,9,10];
                for(var a=0; a < selectAmount; a++){
                    var selected = Math.floor((Math.random() * opts.length));
                    selections.push(opts.splice(selected,1)[0]);
                }

                if(oddOrEven == 'odd'){
                    $scope.selectedOddOptions = selections;
                    $scope.$root.$broadcast('oddSelections')
                }else{
                    $scope.selectedEvenOptions = selections;
                    $scope.$root.$broadcast('evenSelections')
                }
            };

            var addSelection = function($scope, isEven){
                var opts = [1,2,3,4,5,6,7,8,9,10];
                var selectedOpts;
                if(isEven){
                    selectedOpts = $scope.selectedEvenOptions;
                }else{
                    selectedOpts = $scope.selectedOddOptions;
                }

                for(var b=0; b<selectedOpts.length;b++){
                    opts.splice(selectedOpts[b-1],1);
                }

                var newOpt = opts[Math.floor(Math.random() * opts.length)];

                if(isEven){
                   $scope.selectedEvenOptions.push(newOpt);
                }else{
                   $scope.selectedOddOptions.push(newOpt);
                }
                return newOpt
            };

            var toggleOptions = function($scope){
                var removed = 0;
                if($scope.numberOfLines%2==1){
                    if($scope.starter == 'even'){
                        removed = $scope.selectedOddOptions.pop();
                        addSelection($scope, true);
                    }else{
                        removed = $scope.selectedEvenOptions.pop();
                        addSelection($scope, false);
                    }
                }
                return removed
            };

        return {
            restrict: 'E',
            scope: {
                cut: '=',
                selectedEvenOptions: '@',//Jake
                selectedOddOptions: '@', //John
                starter: '@',
                oddTotal: '@',
                evenTotal: '@',
                numberOfLines: '@',
                src_index: '@',
                evenReadyCount: '@',
                oddReadyCount: '@',
                speakerDependents: '@',
                speakerCount: '@',
                controlsVisible: '@',
                range: '@',
                files: '@'
            },
            templateUrl: 'templates/ctoria-loop.html',

            controller: function ($scope, $element, $attrs, AutoGen) {
                $scope.starter = Math.floor((Math.random() * 2)) == 0?'even':'odd';
                $("#speakerToggle").text($scope.starter == 'odd'?'John':'Jake');
                $scope.controlsVisible = true;
                $scope.evenReadyCount = $scope.oddReadyCount = 0;
                $scope.numberOfLines = Math.floor((Math.random() * 5)) + 2;
                if($scope.numberOfLines == 6){
                        $('#increase').toggleClass('disabled', true);
                }else if($scope.numberOfLines == 2){
                        $('#decrease').toggleClass('disabled', true);
                }
                $scope.files = [];
                $scope.speakerDependents = 4;
                $scope.speakerCount = 0;

                var dataFiles = function(type){
                    var $row = $("#row_" + $scope.cut.index);
                    var dataStr = "";
                    switch(type){
                        case 'all':
                            for (var a = 0; a < $scope.numberOfLines-1; a++) {dataStr +=',';}
                            $row.attr('data-file', dataStr);
                            break;
                        case 'odd':
                            var oddData = $row.attr('data-file').split(',');
                            if(oddData.length > ($scope.selectedEvenOptions.length + $scope.selectedOddOptions.length)){
                                oddData.pop();
                            }
                            for(var b=0 ; b < oddData.length; b++){
                                if($scope.starter == 'odd'){
                                    if(b%2==0){oddData[b] ="";}
                                }else{
                                    if(b%2==1){oddData[b] ="";}
                                }
                            }
                            $row.attr('data-file', oddData.toString());
                            break;
                        case 'even':
                            var evenData = $row.attr('data-file').split(',');
                            if(evenData.length > ($scope.selectedEvenOptions.length + $scope.selectedOddOptions.length)){
                                evenData.pop();
                            }
                            for(var c=0 ; c < evenData.length; c++){
                                if($scope.starter == 'even'){
                                    if(c%2==0){evenData[c] ="";}
                                }else{
                                    if(c%2==1){evenData[c] ="";}
                                }
                            }
                            $row.attr('data-file', evenData.toString());
                            break;
                    }
                };

                var createRange = function() {
                    $scope.range = [];
                    if ($scope.starter == 'odd') {
                        if ($scope.numberOfLines % 2 == 1) {
                            $scope.oddTotal = Math.ceil($scope.numberOfLines / 2);
                            $scope.evenTotal = $scope.oddTotal - 1;
                        } else {
                            $scope.oddTotal = $scope.evenTotal = $scope.numberOfLines / 2;
                        }
                    } else if ($scope.starter == 'even') {
                        if ($scope.numberOfLines % 2 == 1) {
                            $scope.evenTotal = Math.ceil($scope.numberOfLines / 2);
                            $scope.oddTotal = $scope.evenTotal - 1;
                        } else {
                            $scope.oddTotal = $scope.evenTotal = $scope.numberOfLines / 2;
                        }
                    }
                    for (var a = 0; a < $scope.numberOfLines; a++) {
                        if ($scope.starter == 'odd') {
                            $scope.range.push(a + 1);
                        } else {
                            $scope.range.push(a + 2);
                        }
                    }
                };
                createRange();

                $scope.toggleSpeaker = function(senderPos){
                    var $speakerToggle = $("#speakerToggle");
                    if($scope.starter == 'even'){
                        $scope.starter = 'odd';
                        $speakerToggle.text('John')
                    }else{
                        $scope.starter = 'even';
                        $speakerToggle.text('Jake');
                    }
                    createRange();
                    var removed = toggleOptions($scope);
                    $scope.$root.$broadcast('toggleStarter', removed);
                    if(senderPos == $scope.cut.index){
                        $scope.$root.$broadcast('incrementDialogueChanges');
                        $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.range[$scope.range.length -1], $scope.cut.index)
                    }
                };

                $scope.decrease = function(){
                    var removed;
                    var index;
                    if($scope.starter == 'odd') {
                        if ($scope.numberOfLines % 2 == 1) {
                            removed = $scope.selectedOddOptions.pop();
                        } else {
                            removed = $scope.selectedEvenOptions.pop();
                        }
                    }else{
                        if ($scope.numberOfLines % 2 == 1) {
                            removed = $scope.selectedEvenOptions.pop();
                        } else {
                            removed = $scope.selectedOddOptions.pop();
                        }
                    }

                    $scope.numberOfLines--;
                    if($scope.numberOfLines == 5){
                        $('#increase').toggleClass('disabled', false);
                    }else if($scope.numberOfLines ==2){
                        $('#decrease').toggleClass('disabled', true);
                    }

                    index = $scope.range[$scope.numberOfLines];
                    createRange();
                    dataFiles(index%2==0?'even':'odd');

                    if($scope.starter == 'odd'){
                        if ($scope.numberOfLines%2==0) {
                            $scope.$root.$broadcast('oddRemove', removed, index);
                        }else{
                            $scope.$root.$broadcast('evenRemove', removed, index);
                        }
                    }else{
                        if ($scope.numberOfLines%2==0) {
                            $scope.$root.$broadcast('evenRemove', removed, index);
                        }else{
                            $scope.$root.$broadcast('oddRemove', removed, index);
                        }
                    }
                    $scope.$root.$broadcast('incrementDialogueChanges');
                    $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.range[$scope.range.length -1], $scope.cut.index)
                };

                $scope.increase = function(){
                    var added;
                    if($scope.starter == 'odd') {
                        if ($scope.numberOfLines % 2 == 1) {
                            added = addSelection($scope, true);
                        }else{
                            added = addSelection($scope, false);
                        }
                    }else{
                        if ($scope.numberOfLines % 2 == 1) {
                            added = addSelection($scope, false);
                        }else{
                            added = addSelection($scope, true);
                        }
                    }
                    $scope.numberOfLines++;
                    if($scope.numberOfLines == 6){
                        $('#increase').toggleClass('disabled', true);
                    }else if($scope.numberOfLines ==3){
                        $('#decrease').toggleClass('disabled', false);
                    }
                    createRange();
                    var index = $scope.range[$scope.numberOfLines-1];
                    dataFiles(index%2==0?'even':'odd');

                    if($scope.starter == 'odd'){
                        if($scope.numberOfLines%2==0){
                            $scope.$root.$broadcast('evenAdd', added, index);
                        }else{
                            $scope.$root.$broadcast('oddAdd', added, index);
                        }
                    }else{
                        if($scope.numberOfLines%2==0){
                            $scope.$root.$broadcast('oddAdd', added, index);
                        }else{
                            $scope.$root.$broadcast('evenAdd', added, index);
                        }
                    }
                    $scope.$root.$broadcast('incrementDialogueChanges');
                    $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.range[$scope.range.length -1], $scope.cut.index)
                };

                $scope.lineRange = function(){
                    return $scope.range;
                };

                $scope.$on('_multifreeReady', function(e, val){
                    if(val%2==0){
                        $scope.evenReadyCount++;
                    }else{
                        $scope.oddReadyCount++;
                    }

                    if($scope.evenReadyCount == $scope.evenTotal){
                         $scope.evenReadyCount = 0;
                         selectOptions($scope, "even", $scope.evenTotal);
                    }

                    if($scope.oddReadyCount == $scope.oddTotal){
                        $scope.oddReadyCount = 0;
                        selectOptions($scope, "odd", $scope.oddTotal);
                    }
                });

                $scope.$on($scope.cut.index+1 + '_speakerChange', function(e, senderPos){
                    $scope.toggleSpeaker(senderPos);
                });

                $scope.$on('sendOptions', function(e,val){
                    if(val%2==0){
                        $scope.$root.$broadcast('evenSelections');
                    }else{
                        $scope.$root.$broadcast('oddSelections');
                    }
                });

                $scope.$on('speakerReady', function(e,val){
                    $scope.speakerCount++;
                    if($scope.speakerCount == $scope.speakerDependents){
                        $scope.speakerCount = 0;
                        AutoGen($scope.cut.index);
                        $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.range[$scope.range.length -1], $scope.cut.index)
                    }
                })
            },
            link: function (scope, $element, $attrs) {
                $element.ready(function () {
                    var $row = $("#row_" + scope.cut.index);
                    var dataStr="";
                    for (var a = 0; a < scope.numberOfLines-1; a++){dataStr += ',';}
                    $row.attr('data-file', dataStr);
                })
            }
        }
    }]);

    scriptBoxWidgets.directive('ctoriaMultiFree', ['cameraFilter', 'speakerFilter',
        function (camera, speaker) {

            var autoSelect = function($scope){
                var $choice = $('[data-cut-id=' + $scope.$parent.cut.index + '_' + $scope.index + ']');
                for(var a=0; a< $scope.selected.length; a++){
                    if(a  == $scope.chosen){
                        var $chosenOpt = $('#multi_' + $scope.index + '_opt_' + $scope.selected[$scope.chosen]);
                        $chosenOpt.css('color', '#ed6a43').toggleClass('optionbtn-disabled');
                        $choice.text($chosenOpt.text());
                    }else{
                        $('#multi_' + $scope.index + '_opt_' + $scope.selected[a]).prop('disabled',true).toggleClass('optionbtn-disabled');//css('color', '#ed6a43')
                    }
                }

                var $camera = $('#camera_' + $scope.$parent.cut.index + '_' + $scope.index);
                var $row = $('#row_' + $scope.$parent.cut.index);
                var $freerow = $('#freerow_' + $scope.index);
                if($scope.chosen < $scope.selected.length){
                    var sources = $scope.cut.options[$scope.selected[$scope.chosen]-1].sources;
                    $scope.src_index = setCamera($camera, sources);
                    var datafiles = $row.attr('data-file').split(',');
                    if($scope.$parent.starter == 'odd'){
                        datafiles[$scope.index -1] = sources[$scope.src_index].file;
                    }else{
                        datafiles[$scope.index -2] = sources[$scope.src_index].file;
                    }
                    $row.attr('data-file', datafiles.toString());
                    $freerow.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                }
            };

            var createChosen = function($scope){
                if($scope.starter == 'odd'){
                    if($scope.index%2==0){
                        $scope.chosen = ($scope.index/2) - 1;
                    }else{
                        $scope.chosen = Math.floor($scope.index/2);
                    }
                }else{
                    if($scope.index%2==1){
                        $scope.chosen = Math.floor($scope.index/2) - 1;
                    }else{
                        $scope.chosen = ($scope.index/2) - 1;
                    }
                }
            };

            var clearSelections = function($scope){
                for(var a=0; a<$scope.selected.length; a++){
                    if(a == $scope.chosen){
                      $('#multi_' + $scope.index + '_opt_' + $scope.selected[$scope.chosen]).css('color','#333');
                    }else{
                      $('#multi_' + $scope.index + '_opt_' + $scope.selected[a]).prop('disabled', false).toggleClass('optionbtn-disabled');
                    }
                }
            };

            var clearRemoved = function($scope, val){
                if($scope.chosen == $scope.selected.length){
                    $('#multi_' + $scope.index + '_opt_' + val).css('color','#333');
                }else{
                    $('#multi_' + $scope.index + '_opt_' + val).prop('disabled', false).toggleClass('optionbtn-disabled');
                }
            };

            return {
            restrict: 'E',
            scope: {
                cut: '=',
                index: '=',
                selected: '@',
                starter:'@',
                chosen: '@',
                file: '@',
                src_index: '@',
                initialised: '@',
                linkColours: '@',
                active: '@',
                visible: '@'
            },
            templateUrl: 'templates/ctoria-multi-free.html',

            controller: function ($scope, $element, $attrs) {

                $scope.initialised = false;
                $scope.starter = $scope.$parent.starter;
                $scope.active = true;
                $scope.linkColours = {'bg':'#6e2982', 'flash': '#d3d8f0'};

                $scope.fold = function (val) {
                    toggleCollapse(val, $scope, true);
                };

                $scope.optionClick = function(newVal){
                    var $row = $('#row_' + $scope.$parent.cut.index);
                    var datafiles = $row.attr('data-file').split(',');
                    var oldVal = $scope.selected[$scope.chosen];
                    var $oldChoice = $('#multi_' + $scope.index + '_opt_' + oldVal);
                    $oldChoice.css('color', '#333');
                    var $newChoice = $('#multi_' + $scope.index + '_opt_' + newVal);
                    $newChoice.css('color', '#ed6a43');
                    $('[data-cut-id=' + $scope.$parent.cut.index + '_' + $scope.index + ']').text($newChoice.text());
                    $scope.selected[$scope.chosen] = newVal;
                    var file = $scope.cut.options[$scope.selected[$scope.chosen]-1].sources[$scope.src_index].file;
                    if($scope.$parent.starter == 'odd'){
                        datafiles[$scope.index -1] = file;
                    }else{
                        datafiles[$scope.index -2] = file;
                    }
                    $row.attr('data-file', datafiles.toString());
                    $scope.$root.$broadcast('incrementDialogueChanges');

                    if($scope.index%2==0){
                        $scope.$root.$broadcast('evenChange', oldVal, newVal, $scope.index)
                    }else{
                        $scope.$root.$broadcast('oddChange', oldVal, newVal, $scope.index)
                    }

                    var togVal = $scope.$parent.cut.index + '_' + $scope.index;
                    toggleCollapse(togVal, $scope, true);
                    if ($('#icon_' + togVal + '> i').hasClass('fa-chevron-right')){
                        $scope.$root.$broadcast('childCollapsed', false);
                    }else{
                        $scope.$root.$broadcast('childCollapsed', true);
                    }
                };

                $scope.$on('toggleStarter', function(e, removed){
                    $scope.starter = $scope.$parent.starter;

                    if($scope.starter == 'even'){
                        if($scope.$parent.numberOfLines%2==1 && $scope.index%2==1){
                            clearRemoved($scope, removed);
                        }

                        if($scope.index == 1){
                            clearSelections($scope);
                            createChosen($scope);
                            return;
                        }

                    }else{

                        if($scope.$parent.numberOfLines%2==1 && $scope.index%2==0){
                            clearRemoved($scope, removed);
                        }

                        if($scope.index == $scope.$parent.numberOfLines + 1) {
                            clearSelections($scope);
                            createChosen($scope);
                            return;
                        }
                    }

                    clearSelections($scope);
                    createChosen($scope);
                    $scope.$root.$broadcast('_multifreeReady', $scope.index);
                });

                if($scope.index%2==0){

                    $scope.$on('evenSelections', function(){
                        $scope.selected = $scope.$parent.selectedEvenOptions;
                        autoSelect($scope);

                    });

                    $scope.$on('evenRemove', function(e, val, index){
                        clearSelections($scope);
                        clearRemoved($scope, val);
                        createChosen($scope);
                        if($scope.index != index) {
                            $scope.$root.$broadcast('_multifreeReady', $scope.index);
                        }
                    });

                    $scope.$on('evenAdd', function(e, val, added){
                        clearSelections($scope);
                        createChosen($scope);
                        $scope.$root.$broadcast('_multifreeReady', $scope.index);
                    });

                    $scope.$on('evenChange', function(e, oldVal, newVal, index){
                        if($scope.index != index){
                            $('#multi_' + $scope.index + '_opt_' + oldVal).prop('disabled', false).toggleClass('optionbtn-disabled');
                            $('#multi_' + $scope.index + '_opt_' + newVal).prop('disabled', true).toggleClass('optionbtn-disabled');
                        }
                    })

                }else{ // odd indexes

                   $scope.$on('oddSelections', function(){
                       $scope.selected = $scope.$parent.selectedOddOptions;
                       autoSelect($scope);
                    });

                    $scope.$on('oddRemove', function(e, val, index){
                        clearSelections($scope);
                        clearRemoved($scope, val);
                        //createChosen($scope);
                        if($scope.index != index) {
                            $scope.$root.$broadcast('_multifreeReady', $scope.index);
                        }
                    });

                    $scope.$on('oddAdd', function(e, val){
                        clearSelections($scope);
                        createChosen($scope);
                        $scope.$root.$broadcast('_multifreeReady', $scope.index);
                    });

                    $scope.$on('oddChange', function(e, oldVal, newVal, index){
                        if($scope.index != index){
                            $('#multi_' + $scope.index + '_opt_' + oldVal).prop('disabled', false).toggleClass('optionbtn-disabled');
                            $('#multi_' + $scope.index + '_opt_' + newVal).prop('disabled', true).toggleClass('optionbtn-disabled');
                        }
                    })
                }
            },

            link: function ($scope, $element, $attrs) {
                $element.ready(function () {
                    createChosen($scope);
                    var $freerow = $('#freerow_' + $scope.index);
                    var $choice = $("[data-cut-id=" + $scope.$parent.cut.index + '_' + $scope.index  + "]");
                    $freerow.on('mouseenter', rowEnter([$freerow, $choice]));
                    $freerow.on('mouseleave', rowLeave([$freerow, $choice]));
                    var $camera = $('#camera_' + $scope.$parent.cut.index + '_' + $scope.index);

                    $camera.on('click', function () {
                        var $row = $('#row_' + $scope.$parent.cut.index);
                        var sources = $scope.cut.options[$scope.selected[$scope.chosen]-1].sources;
                        var datafiles = $row.attr('data-file').split(',');
                        $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                        if($scope.$parent.starter == 'odd'){
                            datafiles[$scope.index -1] = sources[$scope.src_index].file;
                        }else{
                            datafiles[$scope.index -2] = sources[$scope.src_index].file;
                        }
                        $row.attr('data-file', datafiles.toString());
                        $scope.$root.$broadcast('incrementCameraChanges');
                    });
                    $scope.$root.$broadcast('_multifreeReady', $scope.index);
                })
            }
        }
    }]);

    scriptBoxWidgets.directive('ctoriaSpeaker', ['cameraFilter', 'speakerFilter', 'LinkToCut', 'AutoGen',
    function (camera, speaker, LinkToCut, AutoGen, $rootScope) {
        return {
            restrict: 'E',
            scope: {
                cut: '=',
                index: '=',
                selected: '@',
                defSelected: '@',
                selectedChild: '@',
                childCount: '@',
                starter: '@',
                chosen: '@',
                file: '@',
                src_index: '@',
                initialised: '@',
                silent: '@',
                linkColours: '@',
                link: '@'
            },
            templateUrl: 'templates/ctoria-speaker.html',
            controller: function ($scope, $element, $attrs, AutoGen) {
                $scope.initialised = false;
                $scope.linkColours = {'bg':'#6e2982', 'flash': '#d3d8f0'};
                $scope.childCount = 0;

                var switchSelectedChild = function(child, link){
                    var oldSelectedChild = $scope.selectedChild;
                    if($scope.cut.speaker == "different"){
                        if(link == "single" && $scope.link =="false"){ //HACK I know caller 32 is same
                            $scope.selectedChild = child%2==0?2:1;
                        }else {
                            $scope.selectedChild = child%2==0?1:2;
                        }
                        if(!$scope.silent){
                            var $speaker = $('[speaker-cut-id=' + $scope.cut.index + ']');
                            $speaker.text($scope.selectedChild==1?'\u00A0\u00A0Jake:': '\u00A0\u00A0John:');
                        }
                    }else if($scope.cut.speaker == "same"){
                        if(link == "false"){
                            $scope.selectedChild = child%2==0?2:1;
                        }else if(link=="double"){ //HACK  I know my caller 33 is different
                            $scope.selectedChild = child%2==0?1:2;
                            /*var $speaker = $('[speaker-cut-id=' + $scope.cut.index + ']');
                            $speaker.text($scope.selectedChild==1?'Jake:': 'John:');*/
                        }
                    }

                    if($scope.selectedChild != oldSelectedChild){
                        var $oldChildChoice = $('#alt_' + $scope.cut.index + '_child_' + oldSelectedChild);
                        $oldChildChoice.css('color', '#333');
                    }
                };

                var finishLastSpeaker = function(senderPos){
                    var $choice = $('[data-cut-id=' + $scope.cut.index + ']');
                    var $choicerow = $('#choicerow_' + $scope.cut.index);
                    var oldSelectedChild = $scope.selectedChild==1?2:1;
                    var $childChoice = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                    $scope.selected = Number($childChoice.attr('data-selected-position'));
                    var $oldChildChoice = $('#alt_' + $scope.cut.index + '_child_' + oldSelectedChild);
                    var $row = $('#row_' + $scope.cut.index);
                    $row.attr('data-file', $childChoice.attr('data-file'));
                    $choice.text($childChoice.text());
                    $childChoice.css('color', '#ed6a43');
                    $oldChildChoice.css('color', '#333');
                    $choicerow.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                    $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.selectedChild, $scope.selected, senderPos, $scope.link)
                };

                $element.ready(function(){
                    if($scope.cut.content == 'default'){
                        var selectDefault = function(){
                            var $row = $('#row_' + $scope.cut.index);
                            var $choicerow = $('#choicerow_' + $scope.cut.index);
                            var $choice = $('[data-cut-id=' + $scope.cut.index + ']');
                            var $speaker = $('[speaker-cut-id=' + $scope.cut.index + ']');
                            $speaker.text($scope.defSelected == 1? '\u00A0\u00A0Jake:' : '\u00A0\u00A0John:');
                            var file = $scope.cut.options[$scope.defSelected-1].sources[$scope.src_index].file;
                            $row.attr('data-file', file);
                            var $opt = $('#' + $scope.cut.index + '_child_' + $scope.cut.position + '_opt_' + $scope.defSelected);
                            $opt.prop("disabled", true).toggleClass('optionbtn-disabled');
                            var oldSelected = $scope.defSelected==1?2:1;
                            var $oldOpt = $('#' + $scope.cut.index + '_child_' + $scope.cut.position + '_opt_' + oldSelected);
                            $oldOpt.prop("disabled", false).toggleClass('optionbtn-disabled');
                            $choice.text($opt.text());
                            $choicerow.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);
                        };

                        for(var a=0; a< $scope.cut.options.length; a++){
                            var $opt = $('#' + $scope.cut.index + '_child_' + $scope.cut.position + '_opt_' + (a+1));
                            if($scope.cut.options[a].speaker == 'Jake'){
                                $opt.addClass("jake");
                            }else if($scope.cut.options[a].speaker == 'John'){
                                $opt.addClass("john");
                            }
                        }

                        $scope.optionClick = function(selected, senderPos){
                            $scope.defSelected = selected;
                            selectDefault();
                            var $icon = $('#icon_' + $scope.cut.index + '> i');
                            if ($icon.hasClass('fa-chevron-down')) {
                                toggleCollapse($scope.cut.index, $scope, true);
                            }
                            $scope.$root.$broadcast($scope.cut.index + '_speakerChange', senderPos);
                            $scope.$root.$broadcast('incrementDialogueChanges');
                            if(senderPos == $scope.cut.index){
                                $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.defSelected, 0, $scope.cut.index, "false");
                            }
                        };

                        $scope.$on($scope.cut.index-1 + '_lastSpeaker', function(e, val, senderPos){
                            if(senderPos < $scope.cut.index){
                                $scope.defSelected = val%2==0?2:1;
                                if(!$scope.initialised){
                                    var $row = $('#choicerow_' + $scope.cut.index);
                                    var $choice = $('[data-cut-id=' + $scope.cut.index + ']');
                                    $row.on('mouseenter', rowEnter([$row, $choice]));
                                    $row.on('mouseleave', rowLeave([$row, $choice]));
                                    var sources = $scope.cut.options[$scope.defSelected-1].sources;
                                    var $camera = $('#camera_' + $scope.cut.index);
                                    $scope.src_index = setCamera($camera, sources);

                                    $camera.on('click', function(){
                                        var sources = $scope.cut.options[$scope.defSelected-1].sources;
                                        $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                                        var file = $scope.cut.options[$scope.defSelected-1].sources[$scope.src_index].file;
                                        $row.attr('data-file', file);
                                        $scope.$root.$broadcast('incrementCameraChanges');
                                    });
                                    selectDefault();
                                    AutoGen($scope.cut.index);
                                    $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.defSelected, 0, $scope.cut.index, "false");
                                    $scope.initialised = true;
                                    return
                                }
                                selectDefault();
                                $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.defSelected, 0, $scope.cut.index, "false");
                            }
                        });

                        $scope.$on($scope.cut.index+1 + '_lastSpeaker', function(e, selectedChild, selected, senderPos, link){
                            if(senderPos > $scope.cut.index){
                                if(selectedChild == $scope.defSelected){// stops propagation if not switching speaker
                                    $scope.defSelected = $scope.defSelected==2?1:2;//change
                                    $scope.optionClick($scope.defSelected, senderPos); //0, selectedChild);
                                }
                            }
                        });

                    }else{ //alternative speaker
                        $scope.link = $scope.cut.link;
                        $scope.silent = $scope.cut.alternatives[0].speaker == "silent"? true:false;
                        $scope.$on($scope.cut.index + '_speakerChildReady', function(e,val){
                            var $row = $('#choicerow_' + $scope.cut.index);
                            var $choice = $('[data-cut-id=' + $scope.cut.index + ']');
                            $row.on('mouseenter', rowEnter([$row, $choice]));
                            $row.on('mouseleave', rowLeave([$row, $choice]));
                            $scope.childCount++;
                            if($scope.childCount == $scope.cut.alternatives.length){
                                $scope.childCount = 0;
                                $scope.$root.$broadcast('speakerReady');
                            }
                        });

                        $scope.$on($scope.cut.index-1 + '_lastSpeaker', function(e, child, selected, senderPos, link){
                            if(senderPos < $scope.cut.index){
                                switchSelectedChild(child, link);
                                if(link!="false" && $scope.link!="false"){
                                    $scope.$root.$broadcast($scope.cut.index + '_' + $scope.selectedChild + '_pairUp', selected, link, senderPos);
                                    if(!$scope.initialised){
                                        AutoGen($scope.cut.index);
                                        $scope.initialised = true;
                                    }
                                    return
                                }
                                finishLastSpeaker(senderPos);
                                if(!$scope.initialised){
                                    AutoGen($scope.cut.index);
                                    $scope.initialised = true;
                                }
                            }
                        });

                        $scope.$on($scope.cut.index+1 + '_lastSpeaker', function(e, child, selected, senderPos, link){
                            if(senderPos > $scope.cut.index){
                                switchSelectedChild(child, link);
                                if(link!="false" && $scope.link!="false"){
                                    $scope.$root.$broadcast($scope.cut.index + '_' + $scope.selectedChild + '_pairUp', selected, link, senderPos);
                                    return
                                }
                                finishLastSpeaker(senderPos)
                            }
                        });

                        $scope.$on($scope.cut.index + '_childSelected', function(e, val){
                            var propagate = true;
                            if(val.selectedChild != $scope.selectedChild){
                                var $oldChildChoice = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                                $oldChildChoice.css('color', '#333');
                                $scope.selectedChild = val.selectedChild;
                            }else{
                                if(val.linked == "false"){
                                    propagate = false;
                                }
                            }
                            var $row = $('#row_' + $scope.cut.index);
                            var $choicerow = $('#choicerow_' + $scope.cut.index);
                            $row.attr('data-file', val.file);
                            if($scope.cut.speaker == "different" && !$scope.silent){
                                var $speaker = $('[speaker-cut-id=' + $scope.cut.index + ']');
                                $speaker.text($scope.selectedChild==1?'\u00A0\u00A0Jake:': '\u00A0\u00A0John:');
                            }
                            var $choice = $('[data-cut-id=' + $scope.cut.index + ']');
                            var $childChoice = $('#alt_' + $scope.cut.index + '_child_' + $scope.selectedChild);
                            $scope.selected = Number($childChoice.attr('data-selected-position'));
                            $choice.text($childChoice.text());
                            var $icon = $('#icon_' + $scope.cut.index + '> i');
                            if ($icon.hasClass('fa-chevron-down')){
                                $scope.fold($scope.cut.index);
                            }
                            $choicerow.add($choice).animate({backgroundColor: $scope.linkColours.flash}, 150).animate({backgroundColor: '#fbfdff'}, 500);

                            if(propagate){
                                $scope.$root.$broadcast($scope.cut.index + '_lastSpeaker', $scope.selectedChild, $scope.selected, val.sender, $scope.link);
                            }
                        });

                        var $row = $('#row_' + $scope.cut.index);
                        var $camera = $('#camera_' + $scope.cut.index);
                        $camera.on('click', function(){
                            var sources = $scope.cut.alternatives[$scope.selectedChild-1].options[$scope.selected-1].sources;
                            $scope.src_index = cameraToggle(sources, $scope.src_index, $camera, $row);
                            var file = $scope.cut.alternatives[$scope.selectedChild-1].options[$scope.selected-1].sources[$scope.src_index].file;
                            $row.attr('data-file', file);
                            $scope.$root.$broadcast('incrementCameraChanges');
                        });
                    }
                });

                $scope.fold = function(val){
                    toggleCollapse(val, $scope, true);
                };
            },

            link: function ($scope, $element) {
                $element.ready(function () {
                    $scope.$on($scope.cut.index-1 + '_lastSpeaker', function(e,val){
                        if(val%2==0){
                            $scope.selected = 1;
                        }else{
                            $scope.selected = 2;
                        }
                    });
                })
            }
        }
    }]);


scriptBoxWidgets.directive('ctoriaChildSpeakerFree', ['cameraFilter', 'speakerFilter', function(camera,speaker,$rootScope){
    return{
        restrict: 'E',
        scope: {
            selected: '@dataSelectedPosition',
            cut: '=',
            index: '=',
            content: '@',
            file: '@',
            linked: '@',
            cry: '@',
            laugh: '@'
        },

        templateUrl: 'templates/ctoria-child-speaker-free.html',

        controller: function($scope, $element, $rootScope){
            $scope.content = $scope.cut.content;

            $scope.fold = function (val) {
                $('#collapse_' + val).collapse('toggle');
                if ($('#alt_icon_' + val + '> i').hasClass('fa-chevron-right')) {
                    $('#alt_icon_' + val + '> i').addClass('fa-chevron-down').removeClass('fa-chevron-right');
                } else {
                    $('#alt_icon_' + val + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                }
            };

            $scope.$on($scope.index + '_' +$scope.cut.position + '_pairUp', function(e, selected, link, senderPos){
                switch($scope.$parent.link){
                    case "double":
                        var selectedIndex = Math.floor((Math.random()*2));
                        var nextSelection = 0;
                        if(link=="single"){
                            if(selected == 1){
                                var laughOpts = [1,3];
                                nextSelection = laughOpts[selectedIndex]
                            }else if (selected == 2){
                                var cryOpts = [2,4];
                                nextSelection = cryOpts[selectedIndex]
                            }
                        }
                        break;
                    case "single":
                        if(link == "double"){
                            if(selected == 1 || selected == 3){
                                nextSelection = 1
                            }else if(selected == 2 || selected == 4){
                                nextSelection = 2;
                            }
                        }
                        break;
                }
                $scope.optionClick($scope.index + '_child_' + $scope.cut.position + '_opt_' + nextSelection, senderPos);
            });

            $scope.optionClick = function (val, senderPos) {
                var $option = $('#' + val);
                var $choice = $('#alt_' + $scope.index + '_child_' + $scope.cut.position);
                $choice.text($option.text());
                $choice.css('color', '#ed6a43');
                $('#collapse_' + $scope.index + '_child_' + $scope.cut.position).collapse('toggle');
                $('#alt_icon_' + $scope.index + '_child_' + $scope.cut.position + '> i').addClass('fa-chevron-right').removeClass('fa-chevron-down');
                $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected).prop("disabled", false).toggleClass('optionbtn-disabled');
                $scope.selected = Number($option.attr('data-position'));
                $scope.file = $scope.cut.options[$scope.selected-1].sources[$scope.src_index].file;
                $choice.attr('data-file', $scope.file);
                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.attr('data-selected-position', $scope.selected);
                if(senderPos == $scope.index){$scope.$root.$broadcast('incrementDialogueChanges');}
                $rootScope.$broadcast($scope.index + '_childSelected', {selected: $scope.selected, selectedChild: $scope.cut.position, file: $scope.file, linked:$scope.$parent.link, sender:senderPos});
            };

            $element.ready(function(){
                $scope.selected = Math.floor((Math.random() * $scope.cut.options.length) + 1);
                var $choice = $('#alt_' + $scope.index + '_child_' + $scope.cut.position );
                $choice.attr('data-selected-position', $scope.selected);
                var $choice_icon = $('#alt_icon_' + $scope.index + '_child_' + $scope.cut.position );
                var sources = $scope.cut.options[$scope.selected-1].sources;
                var $camera = $('#camera_' + $scope.index);
                $scope.src_index = setCamera($camera, sources);
                $scope.file = sources[$scope.src_index].file;
                $choice.attr('data-file', $scope.file);
                var $option = $('#' + $scope.index + '_child_' + $scope.cut.position + '_opt_' + $scope.selected);
                $choice.text($option.text());

                if($scope.cut.options[$scope.selected-1].speaker == 'Jake'){
                    $choice.addClass("jake");
                }else if($scope.cut.options[$scope.selected-1].speaker == 'John'){
                    $choice.addClass("john");
                }

                $option.prop("disabled", true).toggleClass('optionbtn-disabled');
                $choice.on('mouseenter', rowEnter([$choice, $choice_icon]));
                $choice.on('mouseleave', rowLeave([$choice, $choice_icon]));
                $scope.$root.$broadcast($scope.index + '_speakerChildReady' )
            });
        },

        link: function($scope, $element){
            $element.ready(function () {
            });
        }
    }
}]);

scriptBoxWidgets.directive('showTab',function(){
        return {
            link: function (scope, element, attrs) {
                element.on('click', function(e) {
                    e.preventDefault();
                    console.log('showtabbing fff ' + element);
                    $(element).tab('show');
                });
            }
        };
    });