/**
 * @license videogular v1.2.2 http://videogular.com
 * Two Fucking Developers http://twofuckingdevelopers.com
 * License: MIT
 */
/**
 * @ngdoc directive
 * @name com.2fdevs.videogular.plugins.overlayplay.directive:vgOverlayPlay
 * @restrict E
 * @description
 * Shows a big play button centered when player is paused or stopped.
 *
 * <pre>
 * <videogular vg-theme="config.theme.url" vg-autoplay="config.autoPlay">
 *    <vg-media vg-src="sources"></vg-media>
 *
 *    <vg-overlay-play></vg-overlay-play>
 * </videogular>
 * </pre>
 *
 */
"use strict";
angular.module("com.2fdevs.videogular.plugins.overlayplay", [])
    .run(
    ["$templateCache", function ($templateCache) {
        $templateCache.put("vg-templates/vg-overlay-play",
            '<div class="overlayPlayContainer" ng-click="onClickOverlayPlay()">\
               <div class="main">\
                   <div id="iconBtn" class="iconButton" ng-class="overlayPlayIcon"></div>\
               </div>\
               <div id="movTitle">\
                   <div class="header">{{title}}</div>\
                 <div id="lineCount" class="dialogueControl"><i class="fa fa-exchange"><span id="lineChanges" class="changetext">&nbsp;&nbsp;&nbsp;&nbsp;{{dialogueChanges}}</span></i> </div>\
                 <div id="cameraCount" class="cameraControl"><i class="fa fa-video-camera"><span id="cameraChanges" class="changetext">&nbsp;&nbsp;&nbsp;&nbsp;{{cameraChanges}}</span></i> </div>\
            </div>');
    }]
)
    .directive(
    "vgOverlayPlay",
    ["VG_STATES", "initData", "Play", function (VG_STATES, initData, Play) {
        return {
            restrict: "E",
            require: "^videogular",
            scope: {
                title: '@',
                cameraChanges: '@',
                dialogueChanges: '@'
            },
            templateUrl: function (elem, attrs) {
                return attrs.vgTemplate || 'vg-templates/vg-overlay-play';
            },
            link: function (scope, elem, attr, API) {
                scope.dialogueChanges = 0;
                scope.cameraChanges = 0;
                scope.onChangeState = function onChangeState(newState) {
                    switch (newState) {
                        case VG_STATES.PLAY:
                            scope.overlayPlayIcon = {};
                            var header = $('#movTitle').hide();
                            break;

                        case VG_STATES.PAUSE:
                            scope.overlayPlayIcon = {play: true};
                            var header = $('#movTitle').show();
                            break;

                        case VG_STATES.STOP:
                            scope.overlayPlayIcon = {play: true};
                            var header = $('#movTitle').show();
                            break;
                    }
                };

                scope.onClickOverlayPlay = function onClickOverlayPlay(event) {
                    if(initData.changed){
                        scope.overlayPlayIcon = {};
                        $('#movTitle').hide();
                        Play(true)
                    }else{
                        API.playPause();
                    }
                };

                scope.$on('movieComplete', function(){
                   scope.$root.$broadcast('rebuild', scope.cameraChanges, scope.dialogueChanges)
                });

                scope.overlayPlayIcon = {play: true};

                scope.$watch(
                    function () {
                        return API.currentState;
                    },
                    function (newVal, oldVal) {
                        scope.onChangeState(newVal);
                    }
                );

                scope.$on('incrementCameraChanges', function (e, apply) {
                    scope.cameraChanges++;
                    if(!initData.changed){
                        initData.changed = true;
                    }
                    if(apply==undefined)scope.$apply();
                });

                scope.$on('changeConversation', function(e, data){
                    switch (data){
                        case 'mtl':
                            scope.title = 'Married Too Long';
                            break;
                        case 'sc':
                            scope.title = "Sexual Conundrum";
                            break;
                        case 'ff':
                            scope.title = "Filmic Fantasies";
                            break;
                        case 'eieo':
                            scope.title = "Eat in Eat out";
                            break;
                        case 'bs':
                            scope.title = "Bedtime Stories";
                            break;
                        case 'ofne':
                            scope.title = "Old Films New Endings";
                    }

                    scope.dialogueChanges = scope.cameraChanges = 0;
                });

                scope.$on('resetChanges', function(){
                    console.log('resetting');
                    scope.dialogueChanges = scope.cameraChanges = 0;
                });

                scope.$on('incrementDialogueChanges', function () {
                    if(!initData.changed)initData.changed = true;
                    scope.dialogueChanges++;
                });
            }
        }
    }
    ]);

