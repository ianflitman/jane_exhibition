FROM ubuntu:14.04
RUN apt-get install -y software-properties-common
RUN apt-get install -y wget 
RUN apt-get install -y nano
RUN echo "deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx" >> /etc/apt/sources.list
RUN echo "deb-src http://nginx.org/packages/mainline/ubuntu/ trusty nginx" >> /etc/apt/sources.list
RUN  wget -q -O- http://nginx.org/keys/nginx_signing.key | sudo apt-key add -
RUN add-apt-repository ppa:chris-lea/uwsgi
RUN add-apt-repository ppa:mc3man/trusty-media
RUN add-apt-repository ppa:fkrull/deadsnakes
RUN apt-get update && apt-get install -y \
  nginx\
  uwsgi\
  ffmpeg \
  supervisor \
  build-essential
RUN mkdir -p /home/jane/exhibition/files \
    mkdir -p /home/jane/exhibition/static \
    mkdir -p /home/jane/exhibition/video \
    mkdir -p /home/jane/exhibition/log \
    mkdir -p /home/jane/exhibition/vassals  
RUN apt-get install -y python2.7
RUN apt-get install -y python-pip
RUN apt-get install -y python-dev
#RUN apt-get install -y python-webkit
#RUN apt-get install -y python-gtk2
#RUN apt-get install -y python-gi
#RUN apt-get install -y xvfb
RUN apt-get install -y xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic
ENV DISPLAY :1.0
RUN pip install virtualenv virtualenvwrapper
RUN mkdir -p /home/jane/exhibition/virtualenvs
COPY jvenv-requirements.txt /home/jane/exhibition
ENV WORKON_HOME /home/jane/exhibition/virtualenvs
RUN /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh \
    && mkvirtualenv --system-site-packages jvenv \
    && workon jvenv \
    && pip install -r /home/jane/exhibition/jvenv-requirements.txt"
COPY files/ /home/jane/exhibition/files/
COPY static/ /home/jane/exhibition/static/
COPY video/ /home/jane/exhibition/video/
COPY jane.log /home/jane/exhibition/log
COPY xvfb.log /home/jane/exhibition/log
COPY ffmpeg_creation.log /home/jane/exhibition/log
COPY ffmpeg_failure.log /home/jane/exhibition/log
COPY jane.log /home/jane/exhibition/log
COPY mail.log /home/jane/exhibition/log
COPY delete_dir.log /home/jane/exhibition/log
COPY emperor.log //home/jane/exhibition
COPY jane_uwsgi.sock /home/jane/exhibition
#COPY root /var/spool/cron/crontabs
COPY supervisor-app.conf /etc/supervisor/conf.d
#RUN chown root:crontab /var/spool/cron/crontabs
RUN chown -R www-data:www-data /home
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY run_xvfb.py /home/jane/exhibition/
COPY auto_create.py /home/jane/exhibition/
COPY janeApiServer.py /home/jane/exhibition/
#COPY file_cron.py /home/jane/exhibition/
COPY jane.ini /home/jane/exhibition/vassals
COPY nginx.conf /etc/init
COPY jane.conf /etc/init
RUN mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.save
COPY jane_nginx.conf /etc/nginx/conf.d/
EXPOSE 80
CMD ["supervisord","-n"]

