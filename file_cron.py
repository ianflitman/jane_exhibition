import os
import time
import logging
import errno
import shutil

DIR_PATH = '/home/jane/exhibition/files/'
TIME_INTERVAL = 300  # 5 minutes in seconds or (5 * 60)
MIN_INTERVAL = 5
DELETE_LOG = '/home/jane/exhibition/log/delete_dir.log'


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        file_handler = logging.FileHandler(log_file, mode='w')
        l.addHandler(file_handler)
        return l


def delete_files():
    delete_log = create_logger('delete_log', DELETE_LOG)
    dirlist = [directory for directory in os.listdir(DIR_PATH)]
    time_now = time.time()  # seconds since epoch
    print 'time now: {0}'.format(time_now)
    for d in dirlist:
        print os.stat(DIR_PATH + d).st_ctime  # time of most recent content modification
        print 'time diff: {0}'.format(time_now - os.stat(DIR_PATH + d).st_ctime)
        if time_now - os.stat(DIR_PATH + d).st_ctime > TIME_INTERVAL:
            delete_dir = True  # control variable
            print 'dir {0} is older than {1} mins'.format(d, MIN_INTERVAL)
            delete_log.info(d)
            filelist = [f for f in os.listdir(DIR_PATH + d)]
            for f in filelist:
                print f
                if time_now - os.stat(DIR_PATH + d + '/' + f).st_ctime > TIME_INTERVAL:
                    print '{0} in {1} is older than {2} mins'.format(f, d, MIN_INTERVAL)
                    try:
                        os.remove(DIR_PATH + d + '/' + f)
                        delete_log.info('{0} in {1}'.format(f, d))
                    except OSError as e:
                        if e.errno == errno.EBUSY:
                            delete_log.warn('Cannot delete busy file {0} in {1}'.format(f, d))
                            delete_dir = False  # control variable so directory holding busy file is not deleted
                            pass
            if delete_dir:
                try:
                    shutil.rmtree(DIR_PATH + d)
                except shutil.Error, e:
                    delete_log.warn('Cannot delete directory {0}. Error args: {1}'.format(d, e.args[0]))

delete_files()






